import os

ROOT_PATH: str = os.path.dirname(os.path.abspath(__file__)).replace('\\','/') + '/'
TIME_SERIES_PATH: str = os.path.join(ROOT_PATH, 'openbea/load_input_data/data/')
GRID_PATH: str = os.path.join(ROOT_PATH, 'openbea/grids/data/')
CACHE_PATH: str = os.path.join(ROOT_PATH, 'openbea/cache/')
CONFIG_PATH: str = os.path.join(ROOT_PATH, 'openbea/config/')


RESULT_PATH = os.path.join(ROOT_PATH, 'openbea/results/')

LINOPT_CONSTRAINTS = 'LinOptConstraints'
LINOPT_RESULT_PATH = 'LinOptOutput'

CHARGING_PARK: str = 'charging_park'

TRUTH_CHECKER: [str] = ['true', '1', 'yes', 'yippie', 'yeah', 'ture']