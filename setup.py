from setuptools import setup

setup(
    name='openbea',
    version='0.1.0',
    description='Open Battery Models for Electrical Grid Applications',
    url='https://github.com/dan-kuc/open_BEA',
    author='Daniel Kucevic, Benedikt Tepe, Birgit Schachler',
    author_email='daniel.kucevic@tum.de',
    license='BSD 3-Clause "New" or "Revised" License',
    install_requires=['scipy',
                      'numpy',
                      'pandas',
                      'matplotlib',
                      'h5py',
                      'xlrd',
                      'xlsxwriter',
                      'seaborn',
                      'openpyxl'
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Microsoft :: Windows :: Windows 10',
        'Programming Language :: Python :: 3.6'
        'Topic :: Scientific/Engineering',
    ],
)
