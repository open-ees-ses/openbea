import math
from configparser import ConfigParser

import pandas as pd
import numpy as np
from edisgo import EDisGo
import matplotlib.pyplot as plt
from openbea.analysis_openbea.grid_analysis import GridAnalysis
from openbea.analysis_openbea.load_analysis import LoadAnalysis
from openbea.analysis_openbea.plotting import Plotting
from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.grid_config import GridConfig
from openbea.config.simulation.storage_config import StorageConfig
from openbea.config.simulation.charging_parks_config import ChargingParkConfig
from openbea.storage_integration.simses_integration import SimSES_Integration
from openbea.storage_integration.storage_sizing import StorageSizing
from openbea.storage_integration.storage_dimensioning import MaxPeakShavingLimit
from openbea.storage_integration.storage_reinforce_grid import StorageReinforceGrid
from openbea import settings

class StorageIntegration:
    """
    Storage Integration class
    """

    def __init__(self, result_path: str = None, edisgo: EDisGo = None, simulation_config: ConfigParser = None):
        '''

        Parameters
        ----------
        edisgo :

        '''
        # Edisgo
        self.edisgo = edisgo
        self.__timeindex = self.edisgo.timeseries.timeindex
        self.__timestep = self.__timeindex.freq.delta.seconds
        self.__simulation_steps = len(self.__timeindex)

        #General Settings
        self.__general_config = GeneralConfig(simulation_config)
        self.__save_results = self.__general_config.save_results
        self.__result_path = result_path

        # Storage Variables
        storage_config = StorageConfig(simulation_config)
        storage_reinforce_grid = StorageReinforceGrid()
        self.__equipment_changes = storage_reinforce_grid.extract_grid_reinforcement_positions(edisgo)
        self.__storage_positions_ID = settings.avoided_equipment
        self.__charging_config = ChargingParkConfig(simulation_config)
        self.__simses_validation = self.__general_config.simses_validation
        self.__simulation_config = simulation_config
        self.__grid_config = GridConfig(simulation_config)
        self.__mv_grid_id = self.__grid_config.grid_id
        self.__storage_positions = self.__storage_positions_ID if storage_config.avoid_grid_reinforcement else storage_config.position
        self.__load_analysis = LoadAnalysis(simulation_config)
        self.__remaining_capacities = []

        # calculate highload time hours
        self.__highload = storage_config.high_load_frames()
        if self.__highload: # only calculate exact high load hours if highload peak shaving active
            self.__highload_hours = self._calc_highload_hours(df=
                                                          self.__load_analysis.get_load_profile_at_position
                                                          (self.edisgo, '0'))

        self.__index = 0
        self.__peak_load = 0
        self.__grid_centered = storage_config.grid_centered  # determine if storage optimization based on location or hvmv transformer
        self.__grid_centered_ancilliary = storage_config.grid_centered_ancilliary # check if grid centered is ancilliary or only focus
        self.__powers = self.create_power_dict()

        if storage_config.fixed_storage:
            self.__peak_shaving_limit = storage_config.peak_shaving_limit
            self.__peak_shaving_limit_adapted = self.__peak_shaving_limit[:] # can be overwritten #ISSUETHESHOLD
            self.__efficiency = storage_config.efficiency
            self.__storage_capacity = storage_config.capacity #Ws
            self.__max_power_dch = self.create_max_power_list(capacity_list=self.__storage_capacity,pe_ratio=storage_config.pe_ratio_dch)# W #FIXED #ISSUECAPACITY
            self.__max_power_ch = self.create_max_power_list(capacity_list=self.__storage_capacity,pe_ratio=storage_config.pe_ratio_ch) # W#W #FIXED #ISSUECAPACITY
            self.__start_soc = storage_config.soc
            self.__soc = self.__start_soc
            self.__max_soc = storage_config.max_soc
            self.__min_soc = storage_config.min_soc
            self.__cos_phi = self.__grid_config.cos_phi

        else:
            #df_load = self.__load_analysis.get_load_profile_at_position(self.edisgo, self.__storage_positions[0])
            df_load = self.__powers.get(self.__storage_positions[0])
            self.__efficiency = storage_config.efficiency
            self.__start_soc = storage_config.soc
            self.__soc = self.__start_soc
            self.__max_soc = storage_config.max_soc
            self.__min_soc = storage_config.min_soc
            self.__cos_phi = self.__grid_config.cos_phi

            if storage_config.optimization_algorithm == 'LimitFinder': # capacity given, find possible peak shaving limit
                self._equipment_changes = storage_reinforce_grid.extract_grid_reinforcement_positions(edisgo)
                self._delta_capacity = storage_reinforce_grid.delta_capacity()
                self.__capacity_list = storage_reinforce_grid.set_capacity(self._equipment_changes,self._delta_capacity)
                self.__storage_capacity = self.__capacity_list if storage_config.avoid_grid_reinforcement else storage_config.capacity  # Ws
                self.__max_power_dch = [i * storage_config.pe_ratio_dch / 3600 for i in self.__storage_capacity] # W
                self.__max_power_ch = [i * storage_config.pe_ratio_ch / 3600 for i in self.__storage_capacity] # W
                self.__peak_shaving_limit = []
                self.__peak_shaving_limit_adapted = []

                for i, position in enumerate(self.__storage_positions):

                    limitfinder = MaxPeakShavingLimit(energy=self.__storage_capacity[i],
                                                    power=self.__max_power_dch[i],
                                                    efficiency=self.__efficiency,
                                                    time_step=self.__timestep,
                                                    random_profile=False,
                                                    min_soc=self.__min_soc,
                                                    df=self.__powers.get(self.__storage_positions[i]))

                    peak = limitfinder.calc_max_peak(30)
                    print("Limit finder analyzed that the optimal peak shaving limit is "+str(peak))
                    self.__peak_shaving_limit.append(peak)
                    self.__peak_shaving_limit_adapted.append(peak) # can be overwritten

            else:
                print("STORAGE POSITION AT OPTIMIZATION INIT IS "+str(self.__storage_positions[0]))
                sizing = StorageSizing(df=df_load, timestep=self.__timestep)
                print("SELECTED ALGORITHM IS "+storage_config.optimization_algorithm)
                optimalCapacity, peakLevel = sizing.storageSizing(efficiency=self.__efficiency, per_ch=storage_config.pe_ratio_ch,
                                                                  per_dch=storage_config.pe_ratio_dch
                                                                  , algorithm=storage_config.optimization_algorithm)
                print("THE OPTIMAL CAPACITY IS "+str(optimalCapacity))
                self.__storage_capacity = [optimalCapacity*3600] #Wh to hs
                self.__peak_shaving_limit = [peakLevel]
                self.__peak_shaving_limit_adapted = [self.__peak_shaving_limit] # can be overwritten
                self.__max_power_dch = [self.__storage_capacity[0] * storage_config.pe_ratio_dch / 3600]  # W
                self.__max_power_ch = [self.__storage_capacity[0] * storage_config.pe_ratio_ch / 3600]  # W

    def create_max_power_list(self, capacity_list, pe_ratio):
        max_power_list = []
        for capacity in capacity_list:
            max_power_list.append(capacity * pe_ratio / 3600)
        return max_power_list

    def get_integration_results(self):
        results = {'positions':len(self.__storage_capacity),'storage_capacities':self.__storage_capacity, 'peak_shaving_limits':self.__peak_shaving_limit,
                   'peak_shaving_limits_adapted':self.__peak_shaving_limit_adapted}
        return results

    def get_storage_capacity(self):
        return self.__storage_capacity[0] #ISSUECAPACITY

    def get_storage_degradations(self):
        return self.__remaining_capacities #degradations

    def get_peak_shaving_limit(self):
        return self.__peak_shaving_limit #ISSUETHRESHOLD

    def get_peak_shaving_limit_adapted(self):
        return self.__peak_shaving_limit_adapted #ISSUETHRESHOLD

    def create_power_dict(self): # create a dict of power dataframes
        hvmv_centered = self.__grid_centered
        grid_centered_ancilliary = self.__grid_centered_ancilliary
        powers = {}

        for position in self.__storage_positions:
            if hvmv_centered: # in case of grid centered, scale down power profile from 0 to local
                power_0 = self.__load_analysis.get_load_profile_at_position(self.edisgo, '0')  # in W at 0
                power_orig = self.__load_analysis.get_load_profile_at_position(self.edisgo, position)  # in W

                if grid_centered_ancilliary: # if ancilliary grid-centered: create merged load profile
                    scaling_factor = max(power_orig.values) / max(power_0.values)  # scaling down series based on max value
                    power_0_scaled = power_0.apply(lambda x: x * scaling_factor)
                    power = power_orig.where(power_orig > power_0_scaled, power_0_scaled)
                    '''
                    fig, ax = plt.subplots()
                    ax2 = ax.twinx()
                    ax3 = ax.twinx()
                    power_0_scaled.plot(ax=ax)
                    power_orig.plot(ax=ax2, ls="--", color="red")
                    power.plot(ax=ax3, ls="--", color="green")
                    plt.show()
                    '''
                else: # usual grid-centered: only focus on grid
                    power = power_0

            else: # usual case: use edisgo to retrieve load profile
                power = self.__load_analysis.get_load_profile_at_position(self.edisgo, position)  # in W

            powers.update({position:power})
        return powers

    def build_storage(self) -> EDisGo:
        plotting = Plotting(result_path=self.__result_path, timeindex=self.__timeindex, simulation_config=self.__simulation_config)
        '''Integrate Storage'''
        for i, position in enumerate(self.__storage_positions): # enumerate to access index of items for capacity
            if self.__simses_validation:
                self._simses_setup(position, index=i)

            power_storage_edisgo: pd.DataFrame = self.integrate_storage(position=position, index=i)
            load_profile = self.__load_analysis.get_load_profile_at_position(self.edisgo, position)
            self._add_storage_to_edisgo(power_storage_edisgo, position)

            if self.__simses_validation:
                self._simses_analysis(position)
            else:
                self.__remaining_capacities.append(self.__storage_capacity[i])

            plotting.plot_storage_timeseries(simses_power=None,
                                                    edisgo_power=power_storage_edisgo.S,
                                                    simses_soc=None,
                                                    edisgo_soc=power_storage_edisgo.SOC,
                                                    load_profile=load_profile,
                                                    position=position)

        return self.edisgo

    def integrate_storage(self, position: list, index: int) -> pd.DataFrame:
        power_storage_edisgo = pd.DataFrame({'S': np.empty(self.__simulation_steps),
                                             'P': np.empty(self.__simulation_steps),
                                             'Q': np.empty(self.__simulation_steps),
                                             'SOC': np.empty(self.__simulation_steps)},
                                            index=self.__timeindex)
        self.__soc = self.__start_soc
        self.__index = 0

        power = self.__powers.get(position)

        self.__peak_load = max(power.values) * self.__peak_shaving_limit[index] #ISSUETHRESHOLD
        self.__peak_shaving_limit_adapted[index] = self._find_peak_shaving_limit(power.values, index=index) #ISSUETHRESHOLD

        if self.__peak_shaving_limit_adapted[index] < self.__peak_shaving_limit[index]: #ISSUETHRESHOLD
            print('Required peak shaving limit '
                  + str(self.__peak_shaving_limit[index] * 100) #ISSUETHRESHOLD
                  + ' % is to low for the defined storage system with a maximum power of '
                  + str(self.__max_power_dch[index]) + ' kVA. Peak shaving limit is set to ' #INDIRECTISSUE
                  + str(self.__peak_shaving_limit_adapted[index] * 100) + ' %') #ISSUETHRESHOLD
        self.__peak_load = max(power.values) * self.__peak_shaving_limit_adapted[index] #ISSUETHRESHOLD

        if self.__peak_load < 0:
            raise Exception('Peak_load is below 0. Rethink your peak shaving limit')
        i = 0
        while i < self.__simulation_steps:
            time = self.__timeindex[i]
            power_storage_edisgo.S[time] = self._get_apparent_power(power.iloc[i], time=time, index=index)
            i += 1

        power_storage_edisgo['P'] = power_storage_edisgo.S * self.__cos_phi
        power_storage_edisgo['Q'] = power_storage_edisgo.S * math.sin(math.acos(self.__cos_phi))
        power_storage_edisgo['SOC'] = self._get_soc(power_storage_edisgo.S, index=index)

        return power_storage_edisgo

    def _add_storage_to_edisgo(self, storage_power: pd.DataFrame, position: str) -> None:
        if position == '0':
            bus = self.edisgo.topology.buses_df.index[0]
        else:
            if self.__grid_config.mv_only:
                position = position + '_MV'
            elif position.find('_') == -1:
                position = position + '_1'
            else:
                position = self.__mv_grid_id +'_lvgd_' + position[:position.find('_')] + '_loa' + position[position.find('_'):]
            for buses in self.edisgo.topology.buses_df.index:
                if str(buses).endswith(position):
                    bus = buses
                    break

        # Add timeseries in MW
        self.edisgo.add_component('StorageUnit', add_ts=True,
                                  p_nom=0.3,
                                  bus=bus,
                                  ts_active_power=-(storage_power.P / 1e6),
                                  ts_reactive_power=-(storage_power.Q / 1e6))

    def _get_apparent_power(self, apparent_power: float, time, index) -> float:
        target_power = (self.__peak_load - apparent_power) # target Power in VA
        if self.__highload == False or time.hour in self.__highload_hours:
            # only discharge if no high load shaving or high load currently
            target_power = target_power
        else:
            target_power = 0

        if self.__simses_validation:
            if target_power > 0 and self.__soc >= (self.__max_soc - 0.01):
                target_power = 0
            elif target_power > 0:
                target_power = min(target_power, self.__max_power_ch[index])
            power_simses, self.__soc = self.simses.simulation_one_step(time.value / 10**9, target_power)
            self.simses_soc.append(self.__soc)
            return power_simses

        if target_power < 0: # Discharge storage: Storage power is negative; peak_factor is negative
            target_power = max(target_power, -self.__max_power_dch[index]) #INDIRECTISSUE
            if self.__soc <= 0:
                #print("At target power "+str(target_power)+" the storage was not discharged since SOC below 0.")
                return 0

            delta_soc = (target_power * self.__timestep) / self.__efficiency / self.__storage_capacity[index] #ISSUECAPACITY

            if self.__soc - delta_soc < 0:
                # Disharge only till the storage is empty
                availaible_power = self.__soc * self.__storage_capacity[index] * self.__efficiency / self.__timestep #ISSUECAPACITY
                self.__soc = 0
                #print("At target power " + str(target_power) + " the storage was discharged but not completely: "+str(availaible_power))
                return availaible_power
            else:
                #print("At target power " + str(target_power) + " the storage was discharged  completely.")

                self.__soc += delta_soc
                return target_power


        elif target_power > 0: # Charge storage: Storage power is positve; peak_factor is positive
            target_power = min(target_power, self.__max_power_ch[index]) #INDIRECTISSUE
            if self.__soc >= self.__max_soc:
                return 0

            delta_soc = (target_power * self.__timestep) * self.__efficiency / self.__storage_capacity[index] #ISSUECAPACITY

            if self.__soc + delta_soc > self.__max_soc:
                # Charge only till the storage is full
                availaible_power = 1.5*(self.__max_soc - self.__soc) * self.__storage_capacity[index] / self.__efficiency / self.__timestep #ISSUECAPACITY
                self.__soc = self.__max_soc
                return availaible_power
            else:
                self.__soc += delta_soc
                return target_power

        else:
            return 0

    def _get_soc(self, apparent_power: pd.DataFrame, index) -> pd.DataFrame:
        if self.__simses_validation:
            return pd.DataFrame(index=self.__timeindex, data=self.simses_soc)

        capacity = self.__storage_capacity[index] #ISSUECAPACITY
        stored_energy = 0
        soc = []
        for power in apparent_power:
            if power * self.__timestep > 0:
                stored_energy += (power * self.__efficiency * self.__timestep)
            else:
                stored_energy += (power / self.__efficiency * self.__timestep)
            soc.append(self.__start_soc + stored_energy/capacity)
        return pd.DataFrame(index=self.__timeindex, data=soc)


    def _simses_setup(self, position: str, index) -> pd.DataFrame:
        self.simses_soc = []
        self.simses = SimSES_Integration(result_path=self.__result_path, storage_id=position, simulation_config=self.__simulation_config)
        timeindex = self.__timeindex

        self.simses.build_general_config(timeindex)
        self.simses.build_ems_config()
        self.simses.build_storage_system_config(max(self.__max_power_dch[index], self.__max_power_ch[index]), self.__storage_capacity[index]) #ISSUECAPACITY #INDIRECTISSUE

        self.simses.set_up_simses_simulation(simulation_name=position)

    def _simses_analysis(self, position: str):
        self.__remaining_capacities.append(self.simses.get_remaining_capacity())
        self.simses.close()
        self.simses.analysis()
        print('Storage system at Position ' + position + ' has been validated with SimSES.')

    def _find_peak_shaving_limit(self, power, index):
        if self.__general_config.charging_park_integration:
            base_load = max(power) - self.__charging_config.peak_load
        else:
            base_load = 0
            
        self.__peak_shaving_limit_adapted[index] = self.__peak_shaving_limit[index] #ISSUETHRESHOLD

        if (max(power) - base_load - self.__peak_load) > self.__max_power_dch[index]: #INDIRECTISSUE
            self.__peak_shaving_limit_adapted[index] = (max(power) - self.__max_power_dch[index]) / max(power) #INDIRECTISSUE #ISSUETHRESHOLD
        ''' # temporarily setted at 0 to avoid bugs, has to be checked
        else:
            self.__peak_shaving_limit_adapted[index] = base_load / max(power) #ISSUETHRESHOLD
        '''
        return max(self.__peak_shaving_limit[index], self.__peak_shaving_limit_adapted[index]) #ISSUETHRESHOLD

    def _calc_highload_hours(self, df: pd.DataFrame):
        df = df.copy() # in order to avoid overwriting

        # enrich series with time column
        df = df.to_frame()
        df["time"] = np.empty(self.__simulation_steps)
        i = 0
        while i < self.__simulation_steps:
            df["time"].iloc[i] = self.__timeindex[i]
            i += 1

        #df["hour"] = df["time"].apply(lambda x: print(x))
        df["hour"] = df["time"].apply(lambda x: x.hour)  # get new column with hours
        max_power = df[df.columns[0]].max()
        min_power = df[df.columns[0]].min()
        decentile = (max_power - min_power) / 20
        decentile_threshold = max_power - decentile  # calculate the threshold
        df["highload"] = df[df.columns[0]].apply(lambda x: "High Load" if x > decentile_threshold else "Normal")

        # create dict with highload hours
        highload_hours = {}
        highload_hour_list = []
        for index, row in df.iterrows():
            if row["highload"] == "High Load":
                if row["hour"] in highload_hours:
                    highload_hours.update({row["hour"]: highload_hours.get(row["hour"]) + 1})
                else:
                    highload_hours.update({row["hour"]: 1})
                    highload_hour_list.append(row["hour"])

        highload_hours_df = pd.DataFrame.from_dict(highload_hours, orient='index')
        return highload_hour_list

