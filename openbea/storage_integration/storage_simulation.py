from configparser import ConfigParser

from openbea.analysis_openbea.grid_analysis import GridAnalysis
from openbea.analysis_openbea.storage_costs import StorageCosts
from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.storage_config import StorageConfig
from openbea.grids.gridparameter import GridParameter
from openbea.storage_integration.linopt_integration import LinOptIntegration
from edisgo import EDisGo

from openbea.storage_integration.storage_integration import StorageIntegration


class StorageSimulation:
    """
    '''Integrate Storage Units'''
    """

    def __init__(self, result_path: str = None, simulation_config: ConfigParser = None, edisgo: EDisGo = None):
        self._simulation_config = simulation_config
        self._genaral_config = GeneralConfig(simulation_config)
        self._storage_config = StorageConfig(simulation_config)
        self._save_results = self._genaral_config.save_results
        self._plot_each_timestep = self._genaral_config.plot_each_step
        self.__result_path = result_path
        self._edisgo = edisgo
        self._storage_costs: list = []



    def start(self) -> EDisGo:
        general_config = self._genaral_config
        simulation_config = self._simulation_config
        storage_config = self._storage_config
        storage_mode: str = general_config.storage_mode
        grid_parameter = GridParameter(result_path = self.__result_path, simulation_config=simulation_config)
        edisgo = self._edisgo
        grid_analysis = GridAnalysis(result_path = self.__result_path, simulation_config=simulation_config)

        if storage_mode.lower() == 'linopt' and general_config.charging_park_integration:
            storage = LinOptIntegration(result_path=self.__result_path, edisgo=edisgo, simulation_config=simulation_config)
            grid_parameter.prepare_lin_opt()  # Constraints
            grid_analysis.prepare_lin_opt(edisgo)  # Results from PFA
        elif storage_mode.lower() == 'peakshaving' or storage_mode.lower() == 'peak_shaving':
            storage = StorageIntegration(result_path=self.__result_path, edisgo=edisgo, simulation_config=simulation_config)
        elif storage_mode.lower() == 'opf':
            edisgo.perform_mp_opf(timesteps=edisgo.timeseries.timeindex, total_storage_capacity=storage_config.capacity)
        else:
            print('!!!No valid storage mode given!!!')

        self.__storage_capacity = storage.get_storage_capacity() / 3600  # retrieve capacity after optimization, Ws to Wh
        self.__peak_shaving_limit = storage.get_peak_shaving_limit()
        edisgo = storage.build_storage()
        max_power = [x * max(storage_config.pe_ratio_ch, storage_config.pe_ratio_dch) / 3600 for x in self._storage_config.capacity]
        soh = [i * 3600 / j for i, j in zip(storage.get_storage_degradations(), storage_config.capacity)]
        costs = StorageCosts(storage_config.position, storage_config.capacity, max_power, soh)
        self._specific_storage_costs = costs.specific_costs()
        self._invest_storage_costs = costs.invest_costs(self._specific_storage_costs)
        self._discounted_storage_costs = costs.discounted_costs(self._invest_storage_costs)
        self.__peak_shaving_limit_adapted = storage.get_peak_shaving_limit()  # peak shaving limit after possible adaption

        return edisgo

    def get_peak_shaving_limit(self):
        return self.__peak_shaving_limit

    def get_peak_shaving_limit_adapted(self):
        return self.__peak_shaving_limit_adapted

    def get_storage_capacity(self):
        return self.__storage_capacity

    def get_invest_storage_costs(self) -> dict:
        return self._invest_storage_costs

    def get_specific_storage_costs(self) -> dict:
        return self._specific_storage_costs

    def get_discounted_storage_costs(self) -> dict:
        return self._discounted_storage_costs