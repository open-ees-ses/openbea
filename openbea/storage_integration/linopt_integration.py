import csv
import math
import os
from configparser import ConfigParser

import pandas as pd
import numpy as np

from edisgo import EDisGo

from openbea.analysis_openbea.load_analysis import LoadAnalysis
from openbea.analysis_openbea.plotting import Plotting
from openbea.config.simulation.grid_config import GridConfig
from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.storage_config import StorageConfig
from openbea.storage_integration.simses_integration import SimSES_Integration
from openbeaconstants import LINOPT_RESULT_PATH, LINOPT_CONSTRAINTS


class LinOptIntegration:
    """
    LinOpt Integration class
    """

    def __init__(self, result_path: str = None, edisgo: EDisGo = None, simulation_config: ConfigParser = None):
        '''

        Parameters
        ----------
        edisgo :

        '''
        # Edisgo
        self.edisgo = edisgo
        self.__timeindex = self.edisgo.timeseries.timeindex
        self.__timestep = self.__timeindex.freq.delta.seconds
        self.__size = len(self.__timeindex)

        #General Settings
        self.__simulation_config = simulation_config
        general_config = GeneralConfig(simulation_config)
        grid_config = GridConfig(simulation_config)
        self.__save_results = general_config.save_results
        self.__result_path: str = result_path
        self.__simses_validation = general_config.simses_validation


        # Storage Variables
        storage_config = StorageConfig(simulation_config)
        self.__storage_positions = storage_config.position
        self.__load_analysis = LoadAnalysis(simulation_config)

        if storage_config.fixed_storage:
            self.__peak_shaving_limit = storage_config.peak_shaving_limit
            self.__efficiency = storage_config.efficiency
            self.__storage_capacity = storage_config.capacity  # Ws
            self.__max_power = self.__storage_capacity * max(storage_config.pe_ratio_dch, storage_config.pe_ratio_ch) \
                               / 3600  # W
            self.__c_rate_ch = storage_config.pe_ratio_ch
            self.__c_rate_dch = storage_config.pe_ratio_dch
            self.__start_soc = storage_config.soc
            self.__soc = self.__start_soc
            self.__max_soc = storage_config.max_soc
            self.__min_soc = storage_config.min_soc
            self.__cos_phi = grid_config.cos_phi
        else:
            print('Function to optimize storage not done yet')

    def build_storage(self) -> EDisGo:
        plotting = Plotting(result_path=self.__result_path, timeindex=self.__timeindex, simulation_config=self.__simulation_config)
        lin_opt_dir = os.path.join(self.__result_path, LINOPT_CONSTRAINTS)
        with open(os.path.join(lin_opt_dir, 'storage_parameters.csv'), "w", newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['capacity'] + [str(self.__storage_capacity)])
            writer.writerow(['soc_min'] + [str(self.__min_soc)])
            writer.writerow(['soc_max'] + [str(self.__max_soc)])
            writer.writerow(['soc_start'] + [str(self.__start_soc)])
            writer.writerow(['c_rate_ch'] + [str(self.__c_rate_ch)])
            writer.writerow(['c_rate_dis'] + [str(self.__c_rate_dch)])
            writer.writerow(['efficiency'] + [str(self.__efficiency)])
        file.close()

        self._start_lin_opt()
        '''Integrate Storage'''
        for position in self.__storage_positions:
            power_storage_linopt: pd.DataFrame = self._get_storage_power(position)
            load_profile = self.__load_analysis.get_load_profile_at_position(self.edisgo, position)

            if self.__simses_validation:
                power_storage_edisgo_validated = self._simses_validation(position, power_storage_linopt)
                # take open_BEA storage timeseries and only check fulfillment factor of simses
                self._add_storage_to_edisgo(power_storage_linopt, position)
                plotting.plot_storage_timeseries(simses_power=power_storage_edisgo_validated.S,
                                                        edisgo_power=power_storage_linopt.S,
                                                        simses_soc=power_storage_edisgo_validated.SOC,
                                                        edisgo_soc=power_storage_linopt.SOC,
                                                        load_profile=load_profile,
                                                        position=position)
            else:
                self._add_storage_to_edisgo(power_storage_linopt, position)
                plotting.plot_storage_timeseries(simses_power=None,
                                                        edisgo_power=power_storage_linopt.S,
                                                        simses_soc=None,
                                                        edisgo_soc=power_storage_linopt.SOC,
                                                        load_profile=load_profile,
                                                        position=position)

        return self.edisgo

    def _start_lin_opt(self):
        try:
            import matlab.engine
            eng = matlab.engine.start_matlab()
            lin_opt_results = os.path.join(self.__result_path, LINOPT_RESULT_PATH) + '/'
            os.mkdir(lin_opt_results)
            eng.linopt(self.__result_path, lin_opt_results, nargout=0)
            eng.quit()
        except:
            print('Matlab engine necessary for linopt integration')

    def _get_storage_power(self, position: str) -> pd.DataFrame:
        lin_opt_results = os.path.join(self.__result_path, LINOPT_RESULT_PATH)
        power_storage_linopt = pd.DataFrame({'S': np.empty(self.__size),
                                             'P': np.empty(self.__size),
                                             'Q': np.empty(self.__size),
                                             'SOC': np.empty(self.__size)},
                                            index=self.__timeindex)
        power_storage_linopt.S = pd.read_csv(os.path.join(lin_opt_results,
                                                          'Storage_power_at_' + position + '.csv'), header=None).values
        #power_storage_linopt.SOC = power_storage_linopt + 0.5
        return power_storage_linopt

    def _add_storage_to_edisgo(self, storage_power: pd.DataFrame, position: str) -> None:
        if position == '0':
            bus = self.edisgo.topology.buses_df.index[0]
        else:
            for buses in self.edisgo.topology.buses_df.index:
                if position in str(buses):
                    bus = buses
                    break

        # Add timeseries in MW
        storage_power.P = storage_power.S * self.__cos_phi
        storage_power.Q = storage_power.S * math.sin(math.acos(self.__cos_phi))
        self.edisgo.add_component('StorageUnit', add_ts=True,
                                  p_nom=(self.__max_power / 1e6),
                                  bus=bus,
                                  ts_active_power=-(storage_power.P / 1e6),
                                  ts_reactive_power=-(storage_power.Q / 1e6 ))

    def _get_soc(self, apparent_power: pd.DataFrame) -> pd.DataFrame:
        '''return linopt SOC'''
        return apparent_power

    def get_storage_capacity(self):
        return self.__storage_capacity

    def get_peak_shaving_limit(self):
        return 0

    def _simses_validation(self, position: str, power_storage_edisgo: pd.DataFrame) -> pd.DataFrame:
        simses = SimSES_Integration(result_path=self.__result_path, storage_id=position, simulation_config=self.__simulation_config)
        timeindex = self.__timeindex
        simses.build_general_config(timeindex)
        simses.build_ems_config()
        simses.build_storage_system_config(self.__max_power, self.__storage_capacity)
        #simses.build_profile_config(timeindex, power_storage_edisgo)
        simses.set_up_simses_simulation(simulation_name=position)

        power_storage_simses = pd.DataFrame({'P': [0.0] * len(timeindex),
                                             'Q': [0.0] * len(timeindex),
                                             'S': [0.0] * len(timeindex),
                                             'SOC': [0.0] * len(timeindex)},
                                            index=timeindex)

        j = 0
        while j < len(timeindex):
            power_storage_simses.S[j], power_storage_simses.SOC[j] = \
                simses.simulation_one_step(power=power_storage_edisgo.S[j], time=(timeindex.T[j].value / 10**9))
            j += 1

        simses.close()
        simses.analysis()

        print('Storage system at Position ' + position + ' has been validated with SimSES.')
        power_storage_simses.P = power_storage_simses.S * self.__cos_phi
        power_storage_simses.Q = power_storage_simses.S * math.sin(math.acos(self.__cos_phi))
        return power_storage_simses

