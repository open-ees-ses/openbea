function linopt(result_path, output_path)
try
    disp('Linopt start')
    %% openBEA Variables
    openBEA = true;
    selpath = result_path;
    output_path = output_path;
    %selpath = uigetdir('C:\Users\kucevic\Documents\Python\openbea\openbea\results');

    addpath('C:\Users\kucevic\Documents\Matlab_Projekte\lp_opt\')

    script_00_main;
    disp('     Linopt finished')
catch
    error('Linopt went wrong')
end