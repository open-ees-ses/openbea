import csv
from configparser import ConfigParser

from simses.main import SimSES
import pandas as pd
import os
import datetime

from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.storage_config import StorageConfig


class SimSES_Integration:
    """
        SimSES Integration builds the basic config
    """
    def __init__( self, result_path: str, storage_id: str, simulation_config: ConfigParser):
        self.__result_path = result_path
        self.__storage_id = storage_id
        self.__simulation_config: ConfigParser = ConfigParser()
        self.__simulation_name: str = ''
        self.__storage_config = StorageConfig(simulation_config)
        self.__general_config = GeneralConfig(simulation_config)
        self.__timestep = 0

    def set_up_simses_simulation(self, simulation_name: str) -> None:
        self.__simulation_name = simulation_name
        self.__simses: SimSES = SimSES(str(self.__result_path + '\\').replace('\\', '/'), simulation_name,
                                       do_simulation=True,
                                       do_analysis=True,
                                       simulation_config=self.__simulation_config)

    def build_general_config(self, timeindex: pd.DatetimeIndex) -> None:
        self.__simulation_config.add_section('GENERAL')
        self.__timestep = timeindex.freq.delta
        self.__simulation_config.set('GENERAL', 'TIME_STEP', str(self.__timestep.seconds))
        self.__simulation_config.set('GENERAL', 'START', str(timeindex.T[0]))
        self.__simulation_config.set('GENERAL', 'END', str(timeindex.T[-1] + self.__timestep))

    def build_ems_config(self) -> None:
        self.__simulation_config.add_section('ENERGY_MANAGEMENT')
        self.__simulation_config.set('ENERGY_MANAGEMENT', 'STRATEGY', str('PowerFollower'))

    def build_storage_system_config(self, max_power: float, energy: float) -> None:
        self.__simulation_config.add_section('STORAGE_SYSTEM')
        self.__simulation_config.set('STORAGE_SYSTEM', 'STORAGE_SYSTEM_AC', 'system_1,' + str(abs(max_power)) + ',600,'
                                                                         'acdc,no_housing,no_hvac')
        self.__simulation_config.set('STORAGE_SYSTEM', 'ACDC_CONVERTER', 'acdc, NottonAcDcConverter, 4')
        # SimSES works with Wh
        self.__simulation_config.set('STORAGE_SYSTEM', 'STORAGE_TECHNOLOGY',
                                     'storage_1, ' + str(energy / 3600) + ', lithium_ion,'
                                     + str(self.__storage_config.cell_type))
        self.__simulation_config.add_section('BATTERY')
        self.__simulation_config.set('BATTERY', 'START_SOC', str(self.__storage_config.soc))
        #self.__simulation_config.set('BATTERY', 'MIN_SOC', str(self.__storage_config.min_soc))
        #self.__simulation_config.set('BATTERY', 'MAX_SOC', str(self.__storage_config.max_soc))

    def build_profile_config(self, timeindex: pd.DatetimeIndex, p_storage_timeseries) -> None:
        self.__simulation_config.add_section('PROFILE')
        self.__simulation_config.set('PROFILE', 'LOAD_SCALING_FACTOR', '1')
        self.__simulation_config.set('PROFILE', 'POWER_PROFILE_DIR', str(self.__result_path).replace('\\','/') + '/')
        self.__simulation_config.set('PROFILE', 'LOAD_PROFILE', self.create_load_profile_csv(timeindex,
                                                                                             p_storage_timeseries))

    def create_load_profile_csv(self, timeindex: pd.DatetimeIndex, power_storage_edisgo) -> str:
        annual_power = sum(power_storage_edisgo.S)
        with open(os.path.join(self.__result_path, self.__storage_id) + '.csv', 'w', newline='') as csvfile:
            timeseries = csv.writer(csvfile, delimiter=',')
            timeseries.writerow(['# Source: TUM - open_BEA'])
            timeseries.writerow(['# Unit: W'])
            timeseries.writerow(['# Sampling in s: ' + str(timeindex.freq.delta.seconds)])
            timeseries.writerow(['# Origin: Timeseries generated in open_BEA'])
            timeseries.writerow(['# Datasets: 1'])
            timeseries.writerow(['# Publishable: Yes'])
            timeseries.writerow(['# Annual load consumption in kWh: ' + str(annual_power)])
            timeseries.writerow('')

            i = 0
            while i < len(power_storage_edisgo):
                timeseries.writerow([timeindex.T[i].value / 10 ** 9 + timeindex.freq.delta.seconds, power_storage_edisgo.S])
                i += 1

        return self.__storage_id + '.csv'

    def simulation(self) -> None:
        self.__simses.run_simulation()

    def simulation_one_step(self, time, power) -> float:
        time_simses = time + self.__timestep.seconds
        self.__simses.run_one_simulation_step(time=time_simses, power=power)
        return self.__simses.state.get(self.__simses.state.AC_POWER_DELIVERED), self.__simses.state.get(self.__simses.state.SOC)

    def get_remaining_capacity(self):
        return self.__simses.state.get(self.__simses.state.CAPACITY)

    def analysis(self) -> None:
        self.__simses.run_analysis()

    def close(self) -> None:
        self.__simses.close()