import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta
from random import randint
from openbeaconstants import RESULT_PATH
import os
import time

class StorageSizing:
    # class initiated with underlying load dataframe at hv-mv transformer or local node
    def __init__(self, df: pd.DataFrame,
                 timestep=5):  # initialize gridmap from path, e.g. 'buses.csv', time is minutes of timestep
        self.__df = pd.DataFrame(df)
        self.__timestep = timestep / 60
        self.__time = self.__timestep / 60  # convert timestep to part of hour in order to realize Wh calculation

    # returns basic evaluation of load profile
    def getLoadProfileAnalysis(self):
        max_power_time = self.__df[self.__df.columns[0]].idxmax()
        min_power = self.__df[self.__df.columns[0]].min()
        max_power = self.__df[self.__df.columns[0]].max()
        average_power = self.__df[self.__df.columns[0]].mean()
        median_power = self.__df[self.__df.columns[0]].median()
        results = {'min_power':min_power, 'max_power':max_power,'average_power':average_power, 'median_power':median_power,
                   'max_power_time':max_power_time}
        return results

    # initiate sizing calculation for desired algorithm, returns optimal capacity in Wh + optimal peak shaving level
    def storageSizing(self, efficiency=float, plotting=False, per_ch=float, per_dch=float, amountLevels = 200, simulatedLevels=150,
                      minimum_efficacy=0.1, algorithm="Efficacy"):  # advanced states used algorithm
        self.__per_ch = per_ch # saving power to energy ratio for charging for further calculations
        self.__per_dch = per_dch # saving power to energy ratio for discharging for further calculations

        max_power = self.__df[self.__df.columns[0]].max()  # highest power value occuring in timeseries

        if algorithm != "Random":
            # analyzing a specified amount of possible peak shaving levels between minimum and maximum power, returns dataframe
            # of peakshaving levels and respective necessary storage sizes
            peaksDf = self.simulatePeakShaving(plotting=False, amountLevels=amountLevels, simulatedLevels=simulatedLevels, per_dch=per_dch)

            # following efficacy calculations needed for all efficacy based algorithms
            max_peak_reduction = peaksDf['peak_reduction'].max() # find peakshaving level with higehst peak reduction
            max_power_change_per_capacity = peaksDf['power_change_per_capacity'].max() # find peak shaving level with highest pcpc
            peaksDf["Scaled_Peak_Reduction"] = peaksDf.apply(lambda x: x['peak_reduction'] / max_peak_reduction, axis=1)
            peaksDf["Scaled_Power_Change_per_Capacity"] = peaksDf.apply(
                lambda x: x['power_change_per_capacity'] / max_power_change_per_capacity, axis=1)

            if algorithm == "PER": # find peak shaving level that fits best to the PER of the storage
                print("PER INITIALIZED")
                # select row with lowest wasted storage because of over- or underdimensioning
                bestRow = peaksDf[peaksDf.overdimension == 0]

            elif algorithm =="Target": # algorithm to achieve a desired target peak reduction
                target = 0.9 # 0.9 target = max power after peak shaving lies at 90%
                max_peak = max_power*target
                print("MAX POWER IS"+str(max_power))
                print("MAX PEAK IS "+str(max_peak))
                bestRow = peaksDf[peaksDf["peakShavingLevel"] <= max_peak] # select suitable row from peaksDf

                max_possible_reduction = (bestRow['storageCapacity'].iloc[0]*self.__per_dch)
                print("MAX POSSIBLE REDUCTION IS "+str(max_possible_reduction))

                required_reduction = max_power-max_peak
                print("REQUIRED REDUCTION IS "+str(required_reduction))

                if(required_reduction>max_possible_reduction): # if necesary, correct for discharge power
                    required_storage = (required_reduction)/self.__per_dch
                    print("NEW STORAGE SIZE IS "+str(required_storage))
                    bestRow['storageCapacity'].iloc[0] = required_storage

            elif algorithm == "Growth": # calculate growth of shaved energy and storage capacity with decreasing peak shaving levels
                # Duplicate peaklevel dataframe to calculate change rates of shaved energy and capacity
                dfsizing = peaksDf
                dfsizing["peakShavingLevel"] = dfsizing.peakShavingLevel.astype(int)
                changeDf = dfsizing.pct_change()
                changeDf = changeDf.fillna(0)  # first row is Nan since no change rate available, fill with 0
                dfsizing['shavedEnergyChange'] = changeDf['shavedEnergy']
                dfsizing['storageCapacityChange'] = changeDf['storageCapacity']
                print("GROWTH INITIALIZED")
                for index, row in dfsizing.iterrows():  # iterate over every row in results dataframe
                    difference = row['storageCapacityChange'] - row[
                        "shavedEnergyChange"]  # calculate difference of growth rates
                    # print("DIFFERENCE AMOUNTS "+str(difference))
                    if difference > 0.025:  # stop when growth rate of capacity is larger than energy growth rate with certain threshold of 0.025 to avoid too early stopping
                        # print("STOPPING CRITERIA:")
                        # print(str(row['storageCapacityChange']))
                        # print(str(row["shavedEnergyChange"]))
                        break
                    else:
                        bestRow = row

            elif algorithm == "Efficacy": # based on combined efficacy metric
                # Calculate efficacy metrics in peaks df
                peaksDf["Combined_Efficiency"] = peaksDf.apply(
                    lambda x: x['Scaled_Peak_Reduction'] + x["Scaled_Power_Change_per_Capacity"], axis=1)
                print("Efficacy INITIALIZED")
                # select row with lowest wasted storage because of over- or underdimensioning
                bestRow = peaksDf[peaksDf["Combined_Efficiency"] == peaksDf["Combined_Efficiency"].max()]

            elif algorithm == "Minimum Efficacy": # takes first row over certain power change per capacity threshold
                peaksDf = peaksDf.sort_values(by="Scaled_Power_Change_per_Capacity", ascending=True)
                bestRow = peaksDf[peaksDf["Scaled_Power_Change_per_Capacity"] > minimum_efficacy]

            elif algorithm == "Random": # random guess handled seperately, since optimal capacity calculation isnt based on best row
                pass
            else:  # alternative approach: find point where growth rate of capacity oversteps shaved growth rate
                raise Exception("Please enter an existing peak shaving algorithm.")


            if algorithm == "Growth": # handling: growth works directly, other algorithms with iloc
                optimalPeakShaving = bestRow['peakShavingLevel']  # optimal total peak shvaving in percent
                optimalCapacity = bestRow['storageCapacity']  # optimal storage capacity
                required_per = bestRow['req_per']  # required power to energy ratio in optimum
                shavedPower = bestRow['highestPower'] # required power to energy ratio in optimum
                peakLevel = optimalPeakShaving / max_power  # as decimal
                return optimalCapacity, peakLevel

            else:

                optimalPeakShaving = bestRow['peakShavingLevel'].iloc[0]  # optimal total peak shvaving in percent
                optimalCapacity = bestRow['storageCapacity'].iloc[0]  # optimal storage capacity
                required_per = bestRow['req_per'].iloc[0]  # required power to energy ratio in optimum
                shavedPower = bestRow['highestPower'].iloc[0]  # required power to energy ratio in optimum

                peakLevel = optimalPeakShaving / max_power  # as decimal
                print("[STORAGE SIZING ANALYSIS -"+algorithm+"]")
                print("Optimal Capacity in Wh: " + str(optimalCapacity))
                print("Optimal Peak Shaving Level in W: " + str(optimalPeakShaving))
                print("Optimal Peak Shaving Level in %: " + str(peakLevel))
                print("Highest shaved power in W: " + str(shavedPower))
                print("Required PER in Optimum: " + str(required_per))
                return optimalCapacity, peakLevel

        elif algorithm=="Random": # = algorihm is random
            print("Random INITIALIZED")
            peak_shaving_magnitude = randint(0,int(max_power)) # random peak shaving magnitude
            peakLevel = (max_power-peak_shaving_magnitude)/max_power # calculate peak shaving level as fraction of max power
            optimalCapacity = peak_shaving_magnitude / per_dch # calculate necessary storage size to fulfil desired peak level
            print("Random peak shaving magnitude in W: " + str(peak_shaving_magnitude))
            print("Optimal Peak Shaving Level in %: " + str(peakLevel))
            print("Optimal Capacity in Wh: " + str(optimalCapacity))
            return optimalCapacity, peakLevel

        else:
            raise ValueError("No valid optimization algorithm given.")

    # returns dataframe of possible peak / storage capacity combinations. Input: if plotting desired, amount of total peaks, amount of peaks that shall be analyzed
    def simulatePeakShaving(self, plotting=True, amountLevels=200, simulatedLevels=150, per_ch=float, per_dch=float):
        min_power = self.__df[self.__df.columns[0]].min()
        max_power = self.__df[self.__df.columns[0]].max()

        # calculate amountLevels different shaving levels between min and max power
        difference = (max_power - min_power) / amountLevels  # get quantile value
        i = min_power
        levels = []
        while i < max_power:
            levels.append(i)
            i += difference

        # calculate the respective peak shaving threshold for every power value
        self.__df["threshold"] = self.__df.apply(lambda x: self.getThreshold(levels, x[self.__df.columns[0]]), axis=1)

        results = []
        counter = 0 # count up until desired simulated levels
        # for every possible peak shaving level calculate peaks, required capacity, total shaved energy, highest occuring power during peaks
        for level in reversed(
                levels):
            # reversed order of levels to start with the highest peak shaving level and then to work down
            if counter < simulatedLevels:
                peaks = self.calcPeaks(threshold=level, time=self.__time)
                reqCapacity, highestPower, shavedEnergy = self.analyzePeaks(peaks, plotting=False)
                req_per = highestPower / reqCapacity  # required power to energy ratio to satisfy highest power based on C

                # Needed for PER Calculation: Calculate if storage is overdimensioned (PER-wise)
                if req_per <= per_dch:
                    cap_req = (req_per / per_dch) * reqCapacity  # capacity in order to satisfy highest power
                    overdimension = cap_req - reqCapacity  # overdimensioned capacity to satisfy highest power peak
                    underdimension = 0
                    wastedstorage = overdimension + underdimension

                # Adjust reqCapacity if req_per > highest power -> otherwise, discharge power would be too low
                else:
                    reqCapacity = highestPower / per_dch # Adjust storage size with PER to satisfy highest power
                    overdimension = 0
                    underdimension = reqCapacity * (req_per / per_dch)
                    wastedstorage = overdimension + underdimension

                # Calculate efficiency metrics
                peak_reduction = max_power - level
                power_change_per_capacity = peak_reduction / reqCapacity # efficiency metric

                # combine results of respective peak shaving level in dict entry
                results.append({'peakShavingLevel': level, 'shavedEnergy': shavedEnergy, 'storageCapacity': reqCapacity,
                                'highestPower': highestPower, 'req_per': req_per, 'overdimension': overdimension,
                                'underdimension': underdimension, 'wastedstorage': wastedstorage,
                                'peak_reduction': peak_reduction,
                                'power_change_per_capacity': power_change_per_capacity})
                counter += 1

        peaksDf = pd.DataFrame(results)  # dataframe of highest peaks for every possible peak shaving level

        return peaksDf

    # calculate the respective peak shaving threshold, return it as int
    def getThreshold(self, levels, value):  # Function to order value from power series in right threshold level
        for i in reversed(levels):  # iterate from the top
            if value >= i:
                return int(i)

    # returns list of dicts with all peaks, incl timestamp of beginnign, total power, total to-be-shaved power
    def calcPeaks(self, threshold, time):  # threshold is peak shaving treshold in W, time is timestep in hours
        lastThreshold = 0  # tracks last occured overstepped threshold
        peaks = []  # list of all occurred peaks over predefined threshold

        for index, row in self.__df.iterrows():  # iterate over every row in dataframe of loads
            power = row[self.__df.columns[0]]
            if power > threshold:  # check if power at current level needs to be shaved, else shave power is 0
                shavePower = power - threshold
            else:
                shavePower = 0
            currentThreshold = row['threshold']

            if (lastThreshold == 0 or lastThreshold < threshold) and power > threshold:
                shavePeak = time * (power - threshold)  # calculates to-be-shaved peak in WH
                peaks.append({'energy': power * time,
                              'shavePeak': shavePeak + self.recursiveCoverage(i=index, peak=shavePeak,
                                                                              threshold=threshold),
                              'highestPower': shavePower, 'time': index})
                lastThreshold = currentThreshold

            elif power < threshold:
                lastThreshold = currentThreshold

            else:  # take care of subsequent peak events
                lastEnergy = peaks[-1].get('energy')
                lastShaveEnergy = peaks[-1].get('shavePeak')
                lastHighestPower = peaks[-1].get('highestPower')

                # Check if new highest shave power peak is reached
                if shavePower > lastHighestPower:
                    newHighestPower = shavePower
                else:
                    newHighestPower = lastHighestPower

                newEnergy = lastEnergy + (power * time)
                # calculate new to be shaved energy: last energy + new energy, also implement check if storage can be loaded in intervals after peak through recursiveCoverage
                newShaveEnergy = lastShaveEnergy + time * (power - threshold) + self.recursiveCoverage(i=index,
                                                                                                       peak=shavePeak,
                                                                                                       threshold=threshold)
                peaks[-1].update(energy=newEnergy, shavePeak=newShaveEnergy,
                                 highestPower=newHighestPower)  # update the last entry in peaks dict
                lastThreshold = currentThreshold

        return (peaks)  # power in sets is total peak load in Wh, shavepower is peak load > Wh

    # check if peak can be loaded during upcoming timesteps self, peak, threshold, power, index, required in calcPeaks
    def recursiveCoverage(self, i, peak, threshold):
        try:
            currentTime = i + timedelta(minutes=self.__timestep)  # Determines current timestep of observation
            currentPower = self.__df.loc[currentTime, self.__df.columns[0]]  # get power of current Time
            utilCurrentpower = (threshold - currentPower)*self.__per_ch  # calculate utilizable power, positive if valley, negative if new peak
            utilEnergy = utilCurrentpower * self.__time  # caculate energy that can be used for

            if utilEnergy > peak:  # Case 1: utilEnergy of valley satisfies peak
                return 0
            else:
                if utilEnergy >= 0:  # Case 2: utilEnergy of valley does not satisfies peak, but reduces it
                    return self.recursiveCoverage(currentTime, peak - utilEnergy, threshold)
                else:  # Case 3: New Peak occurs
                    # return positive util energy to get added to new required storage size, rerun recursive coverage
                    return -1 * utilEnergy + self.recursiveCoverage(currentTime, peak - utilEnergy, threshold)

        except Exception as e:
            # print("ERROR AT "+i.strftime('%b %d %Y %H:%M:%S')+str(e))
            return peak

    # calculates highest energy peak (= battery capacity), highest necessary power and total shaved energy for a given peaks dataframe
    def analyzePeaks(self, peaks, plotting=False):
        peakDf = pd.DataFrame(peaks)
        maxRow = peakDf[peakDf.shavePeak == peakDf.shavePeak.max()]  # select row with highest peak in terms of energy
        maxRowPower = peakDf[
            peakDf.highestPower == peakDf.highestPower.max()]  # select row with highest peak in terms of power
        shavePeak = maxRow['shavePeak'].iloc[0]
        shavePower = maxRowPower['highestPower'].iloc[0]
        peakEnergyTotal = peakDf["shavePeak"].sum()

        if plotting:
            # peakDf.plot(x ='time', y='shavePeak', kind = 'bar')
            peakDf.plot(x="time", y=["shavePeak", "energy"], kind="bar")
            plt.show()
            print('The highest peak takes place at ' + maxRow["time"].iloc[0].strftime('%b %d %Y %H:%M:%S') +
                  ' and requires ' + str(maxRow['shavePeak'].iloc[0]) +
                  ' Wh baSttery content to be shaved. Total shaved Energy is ' + str(peakDf["shavePeak"].sum()))
        return shavePeak, shavePower, peakEnergyTotal  # returns highest peak energy, highest occuring power in peaks, and total shaved energy


