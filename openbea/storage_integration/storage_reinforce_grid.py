import pandas as pd
from configparser import ConfigParser
from edisgo import EDisGo
from openbea import settings
from openbea.config.simulation.general_config import GeneralConfig


class StorageReinforceGrid:

    def __init__(self,simulation_config: ConfigParser = None, edisgo: EDisGo = None,result_path: str = None):
        self._simulation_config = simulation_config
        self._general_config = GeneralConfig(simulation_config)
        self._edisgo = edisgo
        self.__result_path = result_path

    def extract_grid_reinforcement_positions(self,edisgo:EDisGo):
        # extract the Position ID's from grid equipment changes
        equipment_changes = []

        reinforce = edisgo.reinforce(copy_graph=True,mode='mv')

        grid_reinforce_costs = reinforce.grid_expansion_costs.total_costs.to_dict()
        for key in grid_reinforce_costs.keys():
            equipment_changes.append(key)

        equipment_changes_id = [i[31:37] for i in equipment_changes]
        return equipment_changes_id

    def set_avoided_equipment(self,equipment_changes:list,equipment_ID):
        avoided_equipment = equipment_changes[equipment_ID]
        return avoided_equipment

    def delta_capacity(self):
        self._capacity_step = 1000000.0 * 3600 # Ws
        self._delta_capacity = 0
        self._delta_capacity = self._capacity_step / (2 ** (settings.iteration_step-1))
        return self._delta_capacity


    def set_capacity(self, equipment_changes:list,delta_capacity_list):
        self._equipment_changes = equipment_changes
        self._start_capacity = settings.actual_capacity
        self._avoided_equipment = settings.avoided_equipment[0]
        if settings.iteration_step > 1:
            if self._avoided_equipment in settings.equipment_changes_after_storage:
                self._capacity = [self._start_capacity + delta_capacity_list]
                return self._capacity
            else:
                self._capacity = [self._start_capacity - delta_capacity_list]
                return self._capacity
        else:
            if self._avoided_equipment in self._equipment_changes:
                self._capacity = [self._start_capacity + delta_capacity_list]
                return self._capacity
            else:
                self._capacity = [self._start_capacity - delta_capacity_list]
                return self._capacity






