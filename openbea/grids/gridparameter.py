import csv
from configparser import ConfigParser

import pandas as pd
import os

from openbea.config.simulation.grid_config import GridConfig
from openbeaconstants import LINOPT_CONSTRAINTS, GRID_PATH

class GridParameter:
    """
       Load grid parameter from eDisgO folder
    """

    def __init__(self, result_path: str = None, simulation_config: ConfigParser = None):
        self.__grid_config = GridConfig(simulation_config)
        self.__result_path = result_path

    def prepare_lin_opt(self):
        lin_opt_dir = os.path.join(self.__result_path, LINOPT_CONSTRAINTS)
        path = os.path.join(GRID_PATH, self.__grid_config.grid_id)
        max_transformer_power = self._get_transformer_nom(path)
        line_constraints = self._get_line_nom(path)
        os.mkdir(lin_opt_dir)
        with open(os.path.join(lin_opt_dir, 'max_transformer_power.csv'), "w") as file:
            writer = csv.writer(file)
            writer.writerow([str(max_transformer_power)])
        file.close()
        line_constraints.to_csv(os.path.join(lin_opt_dir, 'line_constraints.csv'), sep=',')

    def _get_transformer_nom(self, path: str) -> float:
        df = pd.read_csv(os.path.join(path, 'transformers_hvmv.csv'))
        return sum(df.s_nom)

    def _get_line_nom(self, path: str) -> pd.DataFrame:
        df = pd.read_csv(os.path.join(path, 'lines.csv'))
        df = df[['bus0', 'bus1', 's_nom', 'num_parallel']]
        return df