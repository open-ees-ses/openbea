import os
import re
from configparser import ConfigParser
from math import floor
import pandas as pd
from edisgo import EDisGo
from openbea.config.simulation.grid_config import GridConfig
from openbea.config.simulation.general_config import GeneralConfig
from openbea.load_input_data.input_data import InputData
from openbeaconstants import GRID_PATH, CACHE_PATH
try: # only import pickle if available, else pass
    import pickle
except:
    pass

class CreateEdisgo:

    def __init__(self, simulation_config: ConfigParser = None):
        self.__cache = CACHE_PATH
        self.__input_data = InputData(simulation_config)
        self.__grid_config = GridConfig(simulation_config)
        self.__genaral_config = GeneralConfig(simulation_config)
        self.__load_multiplier = self.__genaral_config.load_timeseries_multiplier
        self.__save_results = self.__genaral_config.save_results
        self.__periods = floor(self.__genaral_config.duration / self.__genaral_config.timestep)
        self.__frequency = str(self.__genaral_config.timestep) + 'S'
        self.__start_time = self.__genaral_config.start
        self.__timeindex = pd.DatetimeIndex.empty
        self.__timeseries_load = pd.DataFrame()
        self.__timeseries_generation_fluctuating = pd.DataFrame()
        self.__timeseries_generation_dispatchable = pd.DataFrame()

        self.__identifier = self._create_pickle_name_from_config()

        print('Loading timeseries')
        self._load_timeseries()
        print('     Loading timeseries finished')

    # function to create a pickle name based on the current config - as in grid analysis
    def _create_pickle_name_from_config(self) -> str:
        # collect unique identifiers which enable a differentiation between simulations
        identifiers = [self.__load_multiplier, self.__periods, self.__frequency, self.__start_time
                       ,self.__grid_config.grid_id]

        identification = "PFA"
        for identifier in identifiers:
            identification = identification + re.sub('\D', '', str(identifier))

        return identification

    # checks if pickle for simulation exists
    def pickle_exists(self):
        filename = self.__identifier  # get the unique identifier
        file_path = os.path.join(self.__cache, filename)
        exists = os.path.exists(file_path)
        return exists

    def create_edisgo(self) -> EDisGo:
        # try to open cached pickle dataframe for reduced time

        if not self.__genaral_config.various_timeseries:
            grid = os.path.join(GRID_PATH, self.__grid_config.grid_id)
            # Load multiplier is required at timeseries load becuase there the old annual load is taken for the
            # timeseries [MW] creation
            edisgo = EDisGo(ding0_grid=grid,
                            timeseries_generation_fluctuating=self.__timeseries_generation_fluctuating * 0,
                            timeseries_generation_dispatchable=self.__timeseries_generation_dispatchable * 0,
                            timeseries_load=self.__timeseries_load * self.__load_multiplier,
                            timeindex=self.__timeindex)
            edisgo.topology.loads_df.annual_consumption *= self.__load_multiplier
            return edisgo

        else:
            try:
                print("     Enter open pickle phase")
                filename = self.__identifier # get the unique identifier
                infile = open(os.path.join(self.__cache, filename), 'rb') # open cache folder
                edisgo = pickle.load(infile)
                infile.close()
                print("     Successfully loaded edisgo dataframe from pickle.")
            except Exception as e:
                print("     " + str(e))
                print("     Generate new timeseries")

                grid = os.path.join(GRID_PATH, self.__grid_config.grid_id)
                # Load multiplier is required at timeseries load becuase there the old annual load is taken for the
                # timeseries [MW] creation
                edisgo = EDisGo(ding0_grid=grid,
                                timeseries_generation_fluctuating=self.__timeseries_generation_fluctuating * 0,
                                timeseries_generation_dispatchable=self.__timeseries_generation_dispatchable * 0,
                                timeseries_load=self.__timeseries_load * self.__load_multiplier,
                                timeindex=self.__timeindex)
                edisgo.topology.loads_df.annual_consumption *= self.__load_multiplier

                total_buses = len(edisgo.topology.loads_df.bus)
                i = 1
                if self.__genaral_config.various_timeseries:
                    for bus in edisgo.topology.loads_df.bus:
                        print(str(i) + ' of ' + str(total_buses))
                        i += 1
                        if not bus.endswith('MV'):
                            index = edisgo.topology.loads_df.index[edisgo.topology.loads_df['bus'] == bus][0]
                            if edisgo.topology.loads_df.sector[edisgo.topology.loads_df['bus'] == bus][0] == 'residential':
                                load = self.__input_data.get_timeseries_residential()
                            else: #industry
                                load = self.__input_data.get_timeseries_industry()

                            annual_consumption = \
                                edisgo.topology.loads_df.annual_consumption[edisgo.topology.loads_df['bus'] == bus][0]

                            real2imac = edisgo.timeseries.loads_reactive_power[index].max() \
                                          / edisgo.timeseries.loads_active_power[index].max()

                            edisgo.timeseries._loads_active_power[index] = load.get_values() * annual_consumption
                            edisgo.timeseries._loads_reactive_power[index] = load.get_values() * annual_consumption * real2imac

            return edisgo

    def _load_timeseries(self) -> None:
        '''Load Timeseries'''
        self.__timeindex = self.__input_data.get_timeindex()
        self.__timeseries_load = self.__input_data.get_timeseries_load()
        self.__timeseries_generation_fluctuating = self.__input_data.get_timeseries_generation_fluctuating()
        self.__timeseries_generation_dispatchable = self.__input_data.get_timeseries_generation_dispatchable()