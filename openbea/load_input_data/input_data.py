from configparser import ConfigParser
from math import floor

import pandas as pd
import numpy as np
import os
import h5py
import datetime as dt

from openbea.config.simulation.charging_parks_config import ChargingParkConfig
from openbea.config.simulation.general_config import GeneralConfig
from openbeaconstants import CHARGING_PARK, TIME_SERIES_PATH

class InputData:
    """
    Returns and saves Input Timeseries
    """

    def __init__(self, simulation_config: ConfigParser = None):
        self.__genaral_config = GeneralConfig(simulation_config)
        self.__charging_config = ChargingParkConfig(simulation_config)
        self.__save_results = self.__genaral_config.save_results
        self.__periods = floor(self.__genaral_config.duration / self.__genaral_config.timestep)
        self.__frequency = str(self.__genaral_config.timestep) + 'S'
        self.__start_time = self.__genaral_config.start
        self.__timeseries_path = TIME_SERIES_PATH
        self.__timeindex = pd.date_range(self.__start_time, periods=self.__periods, freq=self.__frequency)
        self.__number_of_charging_parks = len(self.__charging_config.position)
        self.__cp_annual_load = 0
        self.__cp_peak_load = 0
        self.__number_of_residential_profiles = 74
        self.__number_of_industrial_profiles = 36
        self.__number_of_charging_profile = 3
        self.__number_of_industrial_profiles = 36
        self.__residential_counter = 1
        self.__industrial_counter = 2 # naming starts with 2
        self.__charging_park_counter = 1

    def get_timeindex(self) -> pd.DatetimeIndex:
        '''
        Returns a pandas timeindex depending on starttime, periods and frequency

        Returns
        -------
        timeindex: Pandas timeindex

        '''
        return self.__timeindex

    def edit_timeseries(self, timeseries_load, frequency_raw, header_names) -> pd.DataFrame:
        '''
        Returns a load profile (pandas dataframe) with corrected normalization, frequency, length, timestamp

        Returns
        -------
        timeseries: load profile (pandas dataframe)

        '''

        # Change frequency of data, if desired frequency is different from raw data:
        frequency_desired = int(pd.to_timedelta(self.__frequency).total_seconds())


        if frequency_desired > frequency_raw:
            timeseries_load = \
                pd.DataFrame(timeseries_load.values.reshape(-1,int(frequency_desired/frequency_raw),timeseries_load.shape[1]).mean(1))
        elif frequency_desired < frequency_raw:
            timeseries_load_copy = timeseries_load.copy()
            for col in timeseries_load_copy.columns:
                timeseries_load_copy[col] = np.nan      # changes values of copy to nan


            factor = int(frequency_raw/frequency_desired) # calculate factor of frequencies
            timeseries_load_copy = pd.concat([timeseries_load_copy]*factor) # append nan-entries to existing dataframe
            for col in timeseries_load_copy.columns:
                timeseries_load_copy[col].loc[range(0,len(timeseries_load_copy[col]),factor)] = timeseries_load[col].values    # fill in correct values
                timeseries_load_copy[col] = timeseries_load_copy[col].interpolate()     # interpolation


            timeseries_load = timeseries_load_copy
        timeseries_load = timeseries_load.rename(columns=header_names) # Correct header names



        # From normalized Power (to maximal power) to normalized Energy (to total Energy of year)
        timeseries_load = timeseries_load.div(sum(timeseries_load.values)) / frequency_desired

        # Cut the raw data, if desired number of periods is lower than raw data length:
        if self.__periods < len(timeseries_load):
            datetime_start_time = dt.datetime.strptime(self.__start_time, '%Y-%m-%d %H:%M')
            year_data = str(datetime_start_time.year)
            datetime_begin_of_data = dt.datetime.strptime(year_data+"-01-01 00:00", '%Y-%m-%d %H:%M')
            start_index = (datetime_start_time - datetime_begin_of_data).total_seconds() / frequency_desired
            
            timeseries_load = timeseries_load[int(start_index):(int(start_index) + self.__periods)]
        elif self.__periods > len(timeseries_load):
            raise ValueError('Desired number of periods is bigger than raw data!')

        # Add timestamp as index:
        timeindex_desired = pd.date_range(self.__start_time, periods=self.__periods, freq=self.__frequency)
        timeseries_load = timeseries_load.set_index(timeindex_desired)

        return timeseries_load

    def get_timeseries_load(self) -> pd.DataFrame:
        '''
        Returns a load profile (pandas dataframe) Input data based on paper: https://doi.org/10.1016/j.est.2019.101077

        Returns
        -------
        timeseries: load profile for residential, retail, industrial, agricultural (pandas dataframe)

        '''

        header_names = {0:'residential',1: 'retail', 2:'industrial',3: 'agricultural'}
        # Raw Data Frequency:
        frequency_raw = 1 # in seconds

        # Here, all the data is loaded from hdf5-files
        try:
            industry_load1 = h5py.File(os.path.join(self.__timeseries_path,'industry_profile_1.hdf5'), 'r+')
            industry_load2 = h5py.File(os.path.join(self.__timeseries_path,'industry_profile_2.hdf5'), 'r+')
            industry_load3 = h5py.File(os.path.join(self.__timeseries_path,'industry_profile_3.hdf5'), 'r+')
            residential_load1 = h5py.File(os.path.join(self.__timeseries_path, 'residential_profile_28.hdf5'), 'r+')
        except:
            raise FileNotFoundError('No data found')

        # In this dataframe there are the complete load profiles without any timestamp as index
        timeseries_load = pd.DataFrame(
            {'residential': residential_load1['residential_profile'][()],
             'retail':      industry_load1['industry_profile_1'][()],
             'industrial':  industry_load2['industry_profile_2'][()],
             'agricultural':industry_load3['industry_profile_3'][()]
            })

        timeseries_load = self.edit_timeseries(timeseries_load, frequency_raw, header_names)
        timeseries_load = timeseries_load / 51 / 7 * 365 # our timeseries are only 51 weeks
        timeseries_load = timeseries_load * 3600 # * 3600 in 1/h from 1/s
        return timeseries_load

    def get_timeseries_residential(self) -> pd.DataFrame:
        '''
        Returns a specific load profile (pandas dataframe) Input data based on paper:
        https://doi.org/10.1016/j.est.2019.101077 and HTW Berlin
        The load profile changes at each call of this funtion

        Returns
        -------
        timeseries: load profile for residential consumers

        '''

        header_names = {0:'residential'}
        # Raw Data Frequency:
        frequency_raw = 60 # in seconds

        # Here, all the data is loaded from CSV-files
        try:

            csv_name = 'HTW_Family_Loadprofile_2010_60s_' + str(self.__residential_counter) + '.csv'
            residential_load = pd.read_csv(os.path.join(self.__timeseries_path, csv_name))
            residential_load.columns = ['residential']
            start_date = 1262559600
            end_date = 1293404340
            residential_load = residential_load.loc[start_date:end_date]
            residential_load = residential_load.reset_index(drop=True)

        except:
            raise FileNotFoundError('No data found')


        residential_load = pd.DataFrame(residential_load.values / max(residential_load.values))

        timeseries_load = self.edit_timeseries(residential_load, frequency_raw, header_names)
        timeseries_load = timeseries_load / 51 / 7 * 365 # our timeseries are only 51 weeks
        timeseries_load = timeseries_load * 3600 # * 3600 in 1/h from 1/s

        self.__residential_counter += 1 # increase counter by one
        if self.__residential_counter > self.__number_of_residential_profiles:
            self.__residential_counter = 1
            # start again when number of profiles is exceeded

        return timeseries_load

    def get_timeseries_industry(self) -> pd.DataFrame:
        '''
        Returns a specific load profile (pandas dataframe) Input data based on paper:
        https://doi.org/10.1016/j.est.2019.101077
        The load profile changes at each call of this funtion

        Returns
        -------
        timeseries: load profile for industrial consumers

        '''

        header_names = {0:'industrial'}
        # Raw Data Frequency:
        frequency_raw = 60 # in seconds

        # Here, all the data is loaded from hdf5-files
        try:
            # industry_load = h5py.File(os.path.join(self.__timeseries_path, 'industry_profile_1.hdf5'), 'r+')
            csv_name = 'Smart_power_active_load_' + str(self.__industrial_counter) + '.csv'
            industrial_load = pd.read_csv(os.path.join(self.__timeseries_path, csv_name))
            industrial_load.columns = ['industry']
            start_date = 1514761200
            end_date = 1545605940
            industrial_load = industrial_load.loc[start_date:end_date]
            industrial_load = industrial_load.reset_index(drop=True)
        except:
            raise FileNotFoundError('No data found')

        industrial_load = pd.DataFrame(industrial_load.values / max(industrial_load.values))

        timeseries_load = self.edit_timeseries(industrial_load, frequency_raw, header_names)
        timeseries_load = timeseries_load / 51 / 7 * 365 # our timeseries are only 51 weeks
        timeseries_load = timeseries_load * 3600 # * 3600 in 1/h from 1/s

        self.__industrial_counter += 1 # increase counter by one
        if self.__industrial_counter > self.__number_of_industrial_profiles+1: # +1 because naming from 2 to 37
            self.__industrial_counter = 2
            # start again when number of profiles is exceeded -> 2 because naming starts with 2
        return timeseries_load

    def get_timeseries_generation_dispatchable(self) -> pd.DataFrame:
        '''
        Returns a genaration profile (pandas dataframe)

        Returns
        -------
        timeseries: genaration profile for biomass, coal and others (pandas dataframe)

        '''
        timeseries_generation_dispatchable = pd.DataFrame({'biomass': [0.0] * len(self.__timeindex),
                                                           'coal': [0.0] * len(self.__timeindex),
                                                           'other': [0.0] * len(self.__timeindex)},
                                                          index=self.__timeindex)
        return timeseries_generation_dispatchable

    def get_timeseries_generation_fluctuating(self) -> pd.DataFrame:
        '''
        Returns a fluctuation genaration profile (pandas dataframe)

        Returns
        -------
        timeseries: genaration profile for solar and wind (pandas dataframe)

        '''
        timeseries_generation_fluctuating = pd.DataFrame({'solar': [0.0] * len(self.__timeindex),
                                                          'wind': [0.0] * len(self.__timeindex)},
                                                         index=self.__timeindex)
        return timeseries_generation_fluctuating

    def get_timeseries_charging_park_fast(self) -> pd.DataFrame:
        '''
               Returns a load profile (pandas dataframe) Input data based on RLI

               Returns
               -------
               timeseries: load profile for charging park. 1: work, 2: shopping, 3: leisure

               '''
        # Raw Data Frequency:
        frequency_raw = self.__charging_config.freq  # in seconds

        # Here, all the data is loaded from csv-files
        try:
            timeseries_load = pd.read_csv(os.path.join(self.__timeseries_path,
                                                       ("chargepark_" + str(self.__charging_park_counter) + ".csv")))

            if self.__charging_park_counter == self.__number_of_charging_profile:
                self.__charging_park_counter = 1
            else:
                self.__charging_park_counter += 1
        except:
            raise FileNotFoundError('No data found')

        header_names = {timeseries_load.columns.values[0]: CHARGING_PARK}

        self.__cp_annual_load = float(timeseries_load.sum().values * frequency_raw * 1000) # Annual load in Ws -> frequency_raw in seconds
        self.__cp_peak_load = float(max(timeseries_load.values) * 1000) # Peak Load in W

        timeseries_load = self.edit_timeseries(timeseries_load, frequency_raw, header_names)
        return timeseries_load

    def get_timeseries_charging_park_slow(self) -> pd.DataFrame:
        '''
               Returns a load profile (pandas dataframe) Input data based on RLI

               Returns
               -------
               timeseries: load profile for charging park. 1: work, 2: shopping, 3: leisure

               '''
        # Raw Data Frequency:
        frequency_raw = self.__charging_config.freq  # in seconds

        # Here, all the data is loaded from csv-files
        try:
            timeseries_load = pd.read_csv(os.path.join(self.__timeseries_path,
                                                       ("chargepark_slow_" + str(self.__charging_park_counter) + ".csv")))

            if self.__charging_park_counter == self.__number_of_charging_profile:
                self.__charging_park_counter = 1
            else:
                self.__charging_park_counter += 1

        except:
            raise FileNotFoundError('No data found')

        # timeseries_load = pd.concat([timeseries_load] * 55, ignore_index=True)
        # timeseries_load = timeseries_load.truncate(after=525601)

        header_names = {timeseries_load.columns.values[0]: CHARGING_PARK}

        self.__cp_annual_load = float(
            timeseries_load.sum().values * frequency_raw * 1000)  # Annual load in Ws -> frequency_raw in seconds
        self.__cp_peak_load = float(max(timeseries_load.values) * 1000)  # Peak Load in W

        timeseries_load = self.edit_timeseries(timeseries_load, frequency_raw, header_names)
        return timeseries_load

    def get_timeseries_charging_park_smart(self) -> pd.DataFrame:
        '''
               Returns a load profile (pandas dataframe) Input data based on RLI

               Returns
               -------
               timeseries: load profile for charging park. 1: work, 2: shopping, 3: leisure

               '''
        # Raw Data Frequency:
        frequency_raw = self.__charging_config.freq  # in seconds

        # Here, all the data is loaded from csv-files
        try:
            timeseries_load = pd.read_csv(os.path.join(self.__timeseries_path,
                                                       ("chargepark_smart_" + str(self.__charging_park_counter) + ".csv")))

            if self.__charging_park_counter == self.__number_of_charging_profile:
                self.__charging_park_counter = 1
            else:
                self.__charging_park_counter += 1

        except:
            raise FileNotFoundError('No data found')

        # timeseries_load = pd.concat([timeseries_load] * 55, ignore_index=True)
        # timeseries_load = timeseries_load.truncate(after=525601)

        header_names = {timeseries_load.columns.values[0]: CHARGING_PARK}

        self.__cp_annual_load = float(
            timeseries_load.sum().values * frequency_raw * 1000)  # Annual load in Ws -> frequency_raw in seconds
        self.__cp_peak_load = float(max(timeseries_load.values) * 1000)  # Peak Load in W

        timeseries_load = self.edit_timeseries(timeseries_load, frequency_raw, header_names)
        return timeseries_load

    def get_timeseries_charging_park_control_paper(self) -> pd.DataFrame:
        '''
               Returns a load profile (pandas dataframe) Input data based on RLI

               Returns
               -------
               timeseries: load profile for charging park. 1: work, 2: shopping, 3: leisure

               '''
        # Raw Data Frequency:
        frequency_raw = self.__charging_config.freq  # in seconds

        # Here, all the data is loaded from csv-files
        try:
            timeseries_load = pd.read_csv(os.path.join(self.__timeseries_path,
                                                       ("chargepark_" + str(self.__charging_park_counter) + ".csv")))

            if self.__charging_park_counter == self.__number_of_charging_profile:
                self.__charging_park_counter = 1
            else:
                self.__charging_park_counter += 1

        except:
            raise FileNotFoundError('No data found')

        # timeseries_load = pd.concat([timeseries_load] * 55, ignore_index=True)
        # timeseries_load = timeseries_load.truncate(after=525601)

        header_names = {timeseries_load.columns.values[0]: CHARGING_PARK}

        self.__cp_annual_load = float(
            timeseries_load.sum().values * frequency_raw * 1000)  # Annual load in Ws -> frequency_raw in seconds
        self.__cp_peak_load = float(max(timeseries_load.values) * 1000)  # Peak Load in W

        timeseries_load = self.edit_timeseries(timeseries_load, frequency_raw, header_names)
        return timeseries_load

    def get_annual_consumption_charging_park(self) -> float:
        '''
               Returns the annual comsumption of a load profile (float) Input data based on RLI

               Returns
               -------
               float: annual comsumption of a load profile in Ws

               '''
        return self.__cp_annual_load

    def get_peak_load_charging_park(self) -> float:
        '''
               Returns the peak load of a load profile (float) Input data based on RLI

               Returns
               -------
               float: peak load of a load profile in W

               '''
        return self.__cp_peak_load

    def save_timeseries(self, filename: str, timeseries: pd.DataFrame):
        '''
        Saves a specific timeseries to excel

        Parameters
        ----------
        filename : path and filename to save the timeseries
        timeseries : timeseries, which should be saved

        Returns
        -------

        '''
        try:
            timeseries.to_excel(filename)
        except ValueError:
            print('Timeseries too large. Not able to save it')

