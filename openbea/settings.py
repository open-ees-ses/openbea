# Variables for storage capacity optimization to avoid grid reinforcment
# AVOID_GRID_REINFORCEMENT = TRUE

def init():
    global actual_capacity
    actual_capacity = 0
    global equipment_changes_after_storage
    equipment_changes_after_storage = []
    global avoided_equipment
    avoided_equipment = [0]
    global iteration_step
    iteration_step = 1