import os
from configparser import ConfigParser
import pandas as pd

from edisgo import EDisGo

from openbea.analysis_openbea.load_analysis import LoadAnalysis
from openbea.config.simulation.charging_parks_config import ChargingParkConfig
from openbea.config.simulation.general_config import GeneralConfig
from openbeaconstants import CHARGING_PARK
from openbea.load_input_data.input_data import InputData
from openbea.config.simulation.storage_config import StorageConfig

class EMobility:
    def __init__(self, result_path: str = None, simulation_config: ConfigParser = None):
        self.__input_data = InputData(simulation_config)
        self._charging_config = ChargingParkConfig(simulation_config)
        self._general_config = GeneralConfig(simulation_config)
        self._load_analysis = LoadAnalysis(simulation_config)
        self.__nr_charging_park = 0
        self.__result_path = result_path

    def add_component(self, edisgo: EDisGo, position: str) -> EDisGo:
        edisgo = self._add_timeseries(edisgo, position)
        print('     ' + CHARGING_PARK + ' added at id \'' + str(position) + '\' MV-LV busbar')
        return edisgo

    def _add_timeseries(self, edisgo: EDisGo, position: str) -> EDisGo:
        load_id: str = CHARGING_PARK + '_' + str(position)
        integrate_as_storage = False
        hour_to_second = 3600

        if self._charging_config.charging_tech == 'SLOW':
            timeseries_cp = self.__input_data.get_timeseries_charging_park_slow()
        elif self._charging_config.charging_tech == 'SMART':
            timeseries_cp = self.__input_data.get_timeseries_charging_park_smart()
        elif self._charging_config.charging_tech == 'FAST':
            timeseries_cp = self.__input_data.get_timeseries_charging_park_fast()
        elif self._charging_config.charging_tech == 'ControlPaper':
            timeseries_cp = self.__input_data.get_timeseries_charging_park_control_paper()
        else:
            raise Exception('Undefinded value for CHARGING_TECH defined in simulation.local.ini!')

        max_power = timeseries_cp.max()
        avg_power = timeseries_cp.sum()/timeseries_cp.count()
        peakLevel = avg_power/max_power
        StorageConfig.peak_Level = peakLevel
        #print("The peak level is set to: ")
        #print(StorageConfig.peak_Level)

        if self._charging_config.peak_load == 1:
            timeseries_cp_MW = timeseries_cp / 1e3
        else:
            timeseries_cp_MW = timeseries_cp / timeseries_cp.max() * self._charging_config.peak_load / 1e6

        annual_comsuption_cp_MWh = self.__input_data.get_annual_consumption_charging_park() \
                                   / 1e6 / hour_to_second
        timeseries_cp_peak = self.__input_data.get_peak_load_charging_park()

        self.__nr_charging_park += 1

        if integrate_as_storage:
            timeseries_cp_MW = pd.Series(timeseries_cp_MW.T.values[0], index=timeseries_cp_MW.T.columns)
            if position == '0':
                bus = edisgo.topology.buses_df.index[0]
            else:
                for buses in edisgo.topology.buses_df.index:
                    if position in str(buses):
                        bus = buses
                        break

            # Add timeseries in MW
            edisgo.add_component('StorageUnit', add_ts=True,
                                 p_nom=0.0,
                                 bus=bus,
                                 ts_active_power=-(timeseries_cp_MW),
                                 ts_reactive_power=-(timeseries_cp_MW * 0))
        else:
            for names in edisgo.topology.buses_df.index:
                if names.endswith('MV') and position in names:
                    bus = names
                    break

            load_name = edisgo.add_component(comp_type=CHARGING_PARK, load_id=load_id, bus=bus,
                                 peak_load=timeseries_cp_peak,
                                 annual_consumption=annual_comsuption_cp_MWh,
                                 sector='industrial', add_ts=False)

            timeseries_cp_MW.columns = [load_name]
            edisgo.timeseries.loads_active_power = \
                pd.concat(
                    [edisgo.timeseries.loads_active_power,
                     timeseries_cp_MW], axis=1)
            edisgo.timeseries.loads_reactive_power = \
                pd.concat(
                    [edisgo.timeseries.loads_reactive_power,
                     timeseries_cp_MW * 0], axis=1)

        filename = os.path.join(self.__result_path, 'Charging_Park_at_' + str(position) + '.xlsx')

        timeseries_cp_W = timeseries_cp_MW * 1e6
        timeseries_cp_W.to_excel(filename)

        return edisgo