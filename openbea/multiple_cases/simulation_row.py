import warnings
warnings.filterwarnings("ignore")
from openbeaconstants import RESULT_PATH
import pandas as pd
import os
import time
import shutil
from openbea.main import OpenBEA
from configparser import ConfigParser
from openbea.analysis_openbea.plotting import Plotting
import logging
loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
for logger in loggers:
    logger.setLevel(logging.ERROR)


class SimulationRow:

    '''
    Static Config = Config Elements that are the same for every simulation
    Variable Config = Different Configs which are subsequently analyzed

    static_config = list of static config elements in the following format [['STORAGE', 'OPTIMIZATION', 'PER'],...]
    variable_config = list of list of variable config elements in the following format
    [['STORAGE', 'OPTIMIZATION', 'PER'], ['STORAGE', 'OPTIMIZATION', 'EFFICIENCY')]]
    Filename = Str to .csv with config input
    Either pass filename or static config / variable configs
    '''

    def __init__(self, static_config=[], variable_configs=[], filename = ''):
        # Case: Config given through config excel
        if filename != '':
            self.__filename = filename
            self.config_from_csv() # create the config from the csv

        # Otherwise: Config given manually
        else:
            self.__variable_configs = variable_configs
            self.__static_config = static_config


    # Create a Config file based on static config and variable config, which are passed as lists
    def add_configs(self, variable_config=[]):
        config = ConfigParser()
        total_config = self.__static_config + variable_config # create combined config with static and variable elements

        for row in total_config: # row [0]: Section, row[1]: option, row[2]: value
            if not config.has_section(row[0]): # check if config already has respective section
                config.add_section(row[0])
            config.set(row[0],row[1], row[2]) # add config row
        return config

    # Get all Config Values in a dict, useful for result storage
    def configs_to_dict(self, variable_config=[]):
        total_config = self.__static_config + variable_config
        config_dict = {}
        for row in total_config:
            config_item = {"0_CONFIG: "+row[1]:row[2]}
            config_dict.update(config_item)
        return config_dict

    # transform csv file to lists of config elements
    def config_from_csv(self):
        self.__static_config = []
        self.__variable_configs = []

        df = pd.read_csv(self.__filename, delimiter="\t") #load csv from filename, \t as delimiter, if error: try ; or ,
        if df.columns.size == 1:
            df = pd.read_csv(self.__filename, delimiter=';')
            self.__rowcount = df.shape[0]
        if "STORAGE|POSITION" in df.columns: # sort simulation rows by position in order to avoid indexing errors
            df = df.sort_values(by="STORAGE|POSITION")
        for index, row in df.iterrows(): # every line in config df represents either a variable config or static values
            if row['TYPE'] == 'STATIC':  # Add Static Variables to Config which are same in every simulation
                for column in df.columns:  # iterate over all possible columns of config file
                    try:
                        if not pd.isna(row[column]) and column != "TYPE":  # dont include type column
                            section = column.split("|")[0]  # split column names for section and attribute difference
                            attribute = column.split("|")[1]
                            value = str(row[column])
                            # corrections, e.g. position has float point
                            if attribute == "POSITION" and "." in value: # correct for location as float
                                value = value.split(".")[0]

                            # correct decimal in order to avoid "convert string to float" error
                            if attribute == "PEAK_SHHAVING_LIMIT" and "," in value:  # correct for location as float
                                value = value.replace(",", ".")

                            config_values = [section,attribute,value]
                            self.__static_config.append(config_values)
                    except:
                        pass


            elif row['TYPE'] == "VARIABLE":  # Add Dynamic Variables to Config, which represent different simulation cases
                variable_config = []
                for column in df.columns:  # iterate over all possible columns of config file
                    try:
                        if not pd.isna(row[column]) and column != "TYPE":  # dont include type column
                            section = column.split("|")[0]  # split column names for section and attribute difference
                            attribute = column.split("|")[1]
                            value = str(row[column])
                            # corrections, e.g. position has float point
                            if attribute == "POSITION" and "." in value: # correct for location as float
                                value = value.split(".")[0]

                            # correct decimal in order to avoid "convert string to float" error
                            if attribute == "PEAK_SHHAVING_LIMIT" and "," in value:  # correct for location as float
                                value = value.replace(",", ".")

                            config_values=[section, attribute, value]
                            variable_config.append(config_values)
                    except:
                        pass
                self.__variable_configs.append(variable_config)

            else:
                pass

    # Check if a certain query (e.g. POSITION) is already in Config (looks for Attribute, not section)
    def check_if_in_config(self, config: list, query: str):
        for item in config:
            if item[1] == query:
                return True
        return False

    # Read out config value for certain query
    def get_value_from_config(self, config:list,query: str):
        for item in config:
            if item[1] == query: # checks if attribute matches query
                return item[2] # returns value


        '''
        Reading out SytemTechnicalEvaluation.csv for battery on given run, dependent on location and index of run
        location = 0 # storage location, has to be str
        index = 0  # the how manyth storage run on this location, has to be int 
        '''
    def get_battery_results(self, multi_loc: bool, location="0", index=0):
        path = os.path.join(self.__result_path, str(location))
        # retrieve list of all folders within path
        folders = [os.path.join(path, file) for file in os.listdir(path) if os.path.isdir(os.path.join(path, file))]
        target = folders[index]
        technical_evaluation_file = os.path.join(target, 'SystemTechnicalEvaluation0.0.csv')  # get location of eval file
        battery_results = {}  # empty dict for combined battery results
        battery_df = pd.read_csv(technical_evaluation_file)  # open csv as dataframe
        for index, row in battery_df.iterrows():
            battery_eval = {self.create_column_name(multi_loc,"2.1 BATTERY EVALUATION "+row[0], location): row[1]}  # create new dict entry with title as key, result as value
            battery_results.update(battery_eval)  # append value to whole result dict

        lithiumion_evaluation_file = os.path.join(target, 'LithiumIonTechnicalEvaluation1.1.csv')  # get location of eval file
        lithium_df = pd.read_csv(lithiumion_evaluation_file)
        for index, row in lithium_df.iterrows():
            battery_eval = {self.create_column_name(multi_loc,"2.1 LITHIUM ION EVALUATION "+row[0], location): row[1]}  # create new dict entry with title as key, result as value
            battery_results.update(battery_eval)  # append value to whole result dict

        return battery_results

    # load lineload from results
    def get_lineload_results(self, location="0"):
        lineload = os.path.join(self.__result_path, 'Available_Lineload_at_' + location + '.xlsx')  # get location of eval file
        lineload_with_storage = os.path.join(self.__result_path, 'Available_Lineload_with_storage_at_' + location + '.xlsx')
        df = pd.read_excel(lineload)
        df2 = pd.read_excel(lineload_with_storage)
        min_lineload = df[df.columns[1]].min() # get highest lineload before storage
        min_lineload_with_storage = df2[df2.columns[1]].min() # get highest lineload after storage
        lineload_reduction = min_lineload - min_lineload_with_storage
        lineload_results = {}
        lineload_results.update({"3 LINELOAD EVALUATION: Min Available Lineload":min_lineload})
        lineload_results.update({"3 LINELOAD EVALUATION: Min Available Lineload with Storage": min_lineload_with_storage})
        lineload_results.update({"3 LINELOAD EVALUATION: Lineload Reduction": lineload_reduction})
        return lineload_results

    # calculate all test cases from variable configs, save results to excel file
    def start_analysis(self, multiple_simulation_name: str = None):
        self.__start_time = time.time()
        if multiple_simulation_name is None: # check if simulation name is given, if not use current date
            self.__multiple_simulation_name =  str(time.ctime()) # load current date as simulation name
        else:
            self.__multiple_simulation_name = multiple_simulation_name

        self.__results = []
        try:
            i = 0 # index of run, later needed to retrieve battery results
            self.__max_powers_after = [] # empty list for power series for plotting
            for var_config in self.__variable_configs: # iterate over all variable configs given in constructor

                temp_var = []  # temporarlily added config elements for initial reference simulation
                # to make sure that same position is compared, it is checked if in var config position is included
                if self.check_if_in_config(var_config, "POSITION"):
                    # get position of var_config and add it to temp_var,
                    temp_var.append(
                        ["STORAGE", "POSITION", self.get_value_from_config(config=var_config, query="POSITION")])

                config_dict = self.configs_to_dict(
                    var_config)  # create a dict of the config values to add it later to results
                print("+++ STORAGE SIMULATION FOR " + str(config_dict) + " +++")
                config = self.add_configs(variable_config=var_config)
                open_bea: OpenBEA = OpenBEA(multiple_simulations=True,
                                            multiple_simulations_name=self.__multiple_simulation_name, simualtion_id=str(i))
                self.__result_path = os.path.join(RESULT_PATH, self.__multiple_simulation_name, str(i))
                # start calculation with given values, return results
                open_bea.start(simulation_config=config)

                result_entry = {}
                results = open_bea.get_simulations_results() # returns 0: List of Positions, 1: List of Powers Before, 2: List of Powers after

                multi_loc = len(results.get('positions'))>2

                for idx, val in enumerate(results.get('positions')):
                        result_entry.update({self.create_column_name(multi_loc,'1.1_RESULTS: power_before ',val):results.get('max_powers')[idx],
                        self.create_column_name(multi_loc, '1.1_RESULTS: power_before lvmv', val):
                                                 results.get('max_powers_lvmv')[idx],
                         self.create_column_name(multi_loc,'1.2 RESULTS: power after ',val):results.get('max_powers_after_storage')[idx],
                         self.create_column_name(multi_loc,'1.3 RESULTS: power change ',val):results.get('max_powers_after_storage')[idx]-results.get('max_powers')[idx],
                         self.create_column_name(multi_loc,'1.0 RESULTS: Storage Capacity ',val):results.get('storage_capacity'),

                         self.create_column_name(multi_loc, '1.2 RESULTS: power after lvmv', val):
                                                 results.get('max_powers_after_storage_lvmv')[idx],
                         self.create_column_name(multi_loc, '1.3 RESULTS: power change lvmv', val):
                                                 results.get('max_powers_after_storage_lvmv')[idx] -
                                                 results.get('max_powers_lvmv')[idx],
                         self.create_column_name(multi_loc, '1.0 RESULTS: Load Sum Before', val):
                                                 results.get('load_sum_before')[idx],
                         self.create_column_name(multi_loc, '1.0 RESULTS: Load Sum After', val):
                                                 results.get('load_sum_after')[idx],
                        '1.0 RESULTS: Final Peak Shaving Limit':results.get('limit')
                        })

                result_entry.update(config_dict)  # append config parameters to dataframe

                # try adding to plotting lists
                try:
                    self.__max_power_before = results.get('max_power_at_0_init')
                    self.__max_powers_after.append(results.get('max_power_at_0_final'))
                except Exception as e:
                    print(str(e))
                    pass

                # try to get battery evaluation, append it to results HAS TO BE FIXED: get_storage location is not valid anymore
                try:
                    location = str(val)  # retrieve storage location of current run
                    battery_results = self.get_battery_results(location=location, multi_loc=multi_loc,index=0)
                    result_entry.update(battery_results)
                    lineload_results = self.get_lineload_results(location=location)
                    result_entry.update(lineload_results)

                except:
                    pass

                self.__results.append(result_entry) # add simulation results to list of results
                i = i + 1 # update index

            results_df = pd.DataFrame(self.__results) # create dataframe of results
            results_df["Simulation Run"] = results_df.index # create index column

            plotpath = os.path.join(RESULT_PATH, self.__multiple_simulation_name,'Evaluation Plots')
            os.mkdir(plotpath) # create new path for plots

            plotting = Plotting(result_path=plotpath) # initiate plotting
            plotting.plot_values(df=results_df, y_list=['1.3 RESULTS: power change at 0',
                                                        '1.2 RESULTS: power after at 0'],x="Simulation Run")
            plotting.plot_power_at_0(power_before=self.__max_power_before, power_after=self.__max_powers_after)

            # delete non-necessary double columns
            try:
                results_df.drop(['1.1_RESULTS: power_before lvmv at 0', '1.2 RESULTS: power after lvmv at 0',
                                 '1.3 RESULTS: power change lvmv at 0', '1.0 RESULTS: Storage Capacity  at 0'], axis = 1, inplace=True)
            except Exception as e:
                print(str(e))
                pass

            # copy input config excel to result folder
            try:
                config_path = os.path.join(RESULT_PATH, self.__multiple_simulation_name, "Input Config")
                os.mkdir(config_path)
                res_path = os.path.join(config_path, self.__filename) # get path of config file
                cur_path = os.getcwd() # get current working directory
                file_path = os.path.join(cur_path, self.__filename)
                shutil.copy(file_path, res_path) # copy file
            except Exception as e:
                print(str(e))
                pass

            self.results_to_excel(results_df=results_df, multiple_simulation_name=self.__multiple_simulation_name) # create Simulation Results File
            self.__stop_time = time.time()
        except KeyboardInterrupt: # in case of keyboard interrupt, save results nonetheless
            self.results_to_excel(multiple_simulation_name=self.__multiple_simulation_name)  # create Simulation Results File
            raise

    def runtime_analysis(self): # prints runtime of simulation; can only be executed after start_analysis
        runtime = self.__stop_time - self.__start_time
        runtime_per_row = runtime / self.__rowcount
        print("MULTIPLE ROW SIMULATION RUNTIME ANALYSIS")
        print("Total runtime in seconds: "+str(runtime))
        print("Total runtime in minutes: " + str(runtime/60))
        print("Total runtime in hours: " + str(runtime / 3600))
        print("")
        print("Total runtime per row in seconds: " + str(runtime_per_row))
        print("Total runtime per row in minutes: " + str(runtime_per_row / 60))
        print("Total runtime per row in hours: " + str(runtime_per_row / 3600))

    # Saves results in Excel File in Results Path
    def results_to_excel(self, results_df, multiple_simulation_name: str = None):
        result_path = RESULT_PATH  # retrieve result path
        filename = os.path.join(result_path, self.__multiple_simulation_name,
                                multiple_simulation_name + '_simulation_results.xlsx')
        results_df.to_excel(filename)

    # in case of single location unified column names are returned, otherwise including location
    def create_column_name(self, multi_lcc: bool, text: str, location):
        if multi_lcc:
            return text+'at '+str(location)
        else:
            if str(location) == "0":
                return text+" at 0"
            else:
                return text+" local"

if __name__ == "__main__":
    '''
    # Build simulation row within code through lists of config elements, rest of config is taken from local config
    static_config = [] # Static Config are config values which are the same for every simulation
    static_config.append(["GENERAL","DURATION","15552000"])
    static_config.append(["GENERAL", "START", "2011-01-01 00:00"])
    static_config.append(["STORAGE", "PEAK_SHAVING_LIMIT", "0.95"])

    #static_config.append(["STORAGE","POSITION","0"])

    variable_configs = [] # Variable Config defines different simulation sets 
    variable_config1 = []
    variable_config1.append(["STORAGE","FIXED_STORAGE","TRUE"])
    variable_config1.append(["STORAGE", "GRID_CENTERED", "TRUE"])
    variable_config1.append(["STORAGE", "POSITION", "131525, 134676, 135931"])
    variable_config1.append(["STORAGE", "CAPACITY", "309522"])
    variable_configs.append(variable_config1)

    variable_config2 = []
    variable_config2.append(["STORAGE", "FIXED_STORAGE", "TRUE"])
    variable_config2.append(["STORAGE", "GRID_CENTERED", "TRUE"])
    variable_config2.append(["STORAGE", "POSITION", "0"])
    variable_config2.append(["STORAGE", "CAPACITY", "928566"])
    variable_configs.append(variable_config2)

    simul = SimulationRow(static_config=static_config,variable_configs=variable_configs)
    simul.start_analysis(multiple_simulation_name="Random Runs 180 Days") # starts simulation row, iterates over all variable configs, saves result excel in RESULTPATH

    #simul.start_analysis()
    '''

    #Or with input file
    simul = SimulationRow(filename="0113 Grid Centered based on Individual Storage Sizing.csv")
    simul.start_analysis()
    simul.runtime_analysis()
    #simul.start_analysis(multiple_simulation_name="Random Runs 180 Days")

