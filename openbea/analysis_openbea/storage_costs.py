from configparser import ConfigParser
from openbea.config.simulation.costs_config import CostsConfig


class StorageCosts:

    def __init__(self, position: list, capacity: list, power: list, soh: list, simulation_config: ConfigParser = None) -> None:
        '''

        Parameters
        ----------
        position : list of all positions
        capacity : list of all storage capacities in Ws
        power : list of all storage maximum powers in W
        soh : list of all storage soh in %
        '''
        self.__capacity = capacity
        self.__power = power
        self.__position = position
        self.__soh = soh

        self._costs_config = CostsConfig(simulation_config)

        self._use_user_defined_costs = self._costs_config.use_user_defined_costs
        self._use_system_costs = self._costs_config.use_system_costs # set if the investment costs are calculated on system or storage level
        self._year = self._costs_config.year_of_investment # year of investment

    def specific_costs(self):
        # The cost fittings functions are valid between 2020 and 2040.
        if self._year <2020.0 or self._year >2040.0:
            raise Exception('Invalid value for YEAR_OF_INVESTMENT defined in simulation.local.ini!')

        # The specific investment costs can be defined by user (TRUE) or are calculated with the cost fitting functions below (FALSE)
        if self._use_user_defined_costs == True:
            specific_investment_costs_energy_storage = self._costs_config.specific_investment_costs_energy_storage
            specific_investment_costs_power = self._costs_config.specific_investment_costs_power
            specific_investment_costs_energy_system = self._costs_config.specific_investment_costs_energy_system
        else:
            # The datapoints and fitting sessions used for the cost fitting functions can be found in openbea/analysis_openbea/data
            # The specific investment costs are calculated with the chosen cost prognosis scenario and the year of investment
            if self._costs_config.predefined_costs_scenario == 'BASE':
                specific_investment_costs_energy_system = 7491.0 / (self._year - 2004.0)
            elif self._costs_config.predefined_costs_scenario == 'LOW':
                specific_investment_costs_energy_system = 5726.0 / (self._year - 2006.0)
            elif self._costs_config.predefined_costs_scenario == 'HIGH':
                specific_investment_costs_energy_system = 9466.0 / (self._year - 2002.0)
            else:
                raise Exception('Undefinded value for PREDEFINED_COSTS_SCENARIO defined in simulation.local.ini!')

        # Teh specific costs can just include the specific energy costs on system level or the specific energy costs on storage level as well as the specific power costs
        if self._use_system_costs == True and self._use_user_defined_costs:
            specific_costs = [specific_investment_costs_energy_system] # Specific energy costs on system level in $/kWh (user defined)
        elif self._use_user_defined_costs == False:
            specific_costs = [specific_investment_costs_energy_system]  # Specific energy costs on system level in $/kWh (by fitting function)
        else:
            specific_costs = [specific_investment_costs_energy_storage,specific_investment_costs_power] # Specific energy costs on storage level in $/kWh, Specific power costs on in $/kW

        return specific_costs

    def invest_costs(self,specific_costs) -> dict:
        investment_costs = {}
        self._investment_costs_energy_storage = {}
        self._investment_costs_power = {}
        self._investment_costs_energy_system = {}
        storage_nr = 0

        # Calculation of investment costs
        # Option_1: Separate calculation of energy storage and power electronics investment costs (FALSE) based on user_defined_costs
        # Option_2: Calculation of system investment costs (TRUE) based on user_defined_costs
        # Option_3: Calculation of system investment costs (TRUE) based on fitting_functions
        for i in self.__position:
            if self._use_system_costs == False and self._use_user_defined_costs == True:
                self._investment_costs_energy_storage[i] = specific_costs[0]*self.__capacity[storage_nr]//3600000 # Investment energy costs on storage level in $, capacity in Ws
                self._investment_costs_power[i] = specific_costs[1]*self.__power[storage_nr]/1000 # Investment power costs in $, capacity in W
                investment_costs[i] = self._investment_costs_energy_storage[i]+self._investment_costs_power[i]
            elif self._use_system_costs == True and self._use_user_defined_costs == True:
                self._investment_costs_energy_system[i] = specific_costs[0] * self.__capacity[storage_nr]/3600000 # Investment energy costs on system level in $, capacity in Ws
                investment_costs[i] = self._investment_costs_energy_system[i]
            else:
                self._investment_costs_energy_system[i] = specific_costs[0] * self.__capacity[storage_nr]/3600000 # Investment energy costs on system level in $, capacity in Ws
                investment_costs[i] = self._investment_costs_energy_system[i]

            storage_nr += 1
            investment_costs[i] = round(investment_costs[i], 2)


        return investment_costs

    def discounted_costs(self,investment_costs):
        discounted_costs = {}
        depreciation_period_storage = self._costs_config.depreciation_period_storage
        discount_rate = self._costs_config.discount_rate
        storage_nr = 0

        for i in self.__position:
                discounted_costs[i] = investment_costs[i]/(1+discount_rate)**depreciation_period_storage # Discounting of the investment costs
        storage_nr +=1

        return discounted_costs

