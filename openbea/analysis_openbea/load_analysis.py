import os
from configparser import ConfigParser

import numpy as np
from edisgo import EDisGo
from edisgo.tools import tools

from openbea.analysis_openbea.grid_analysis import GridAnalysis
from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.grid_config import GridConfig
from openbea.storage_integration.storage_sizing import StorageSizing
import pandas as pd



class LoadAnalysis:
    """
    Basic load Analysis
    """
    def __init__(self, result_path: str = None, simulation_config: ConfigParser = None):
        self.__genaral_config = GeneralConfig(simulation_config)
        self.__grid_config = GridConfig(simulation_config)
        self.__save_results = self.__genaral_config.save_results
        self.__result_path = result_path
        self.__grid_analysis = GridAnalysis(simulation_config)
        self.__transformer_energy_without_cp = 0
        self.__transformer_energy_with_cp = 0
        self.__transformer_energy_with_storage = 0

    def analysis(self, edisgo: EDisGo, position: str, charging_parks: bool = False, storage: bool = False) -> None:
        if self.__grid_config.mv_only:
            #if position is not '0':
            #    lineload = self.__grid_analysis.get_line_lv_load(edisgo, position)
            #    available_lineload = self.__grid_analysis.calculate_available_line_load(edisgo, lineload)
            power: pd.DataFrame = self.get_load_profile_at_position(edisgo, position)

            if not storage and not charging_parks:
                #if position is not '0':
                #    filename = os.path.join(self.__result_path, 'Available_Lineload_at_' + str(position) + '.xlsx')
                #    available_lineload.to_excel(filename)
                filename = os.path.join(self.__result_path, 'Power_at_' + str(position) + '.xlsx')
                power.to_excel(filename)
            elif charging_parks and not storage:
                #if position is not '0':
                #    filename = os.path.join(self.__result_path, 'Available_Lineload_with_CP_at_'
                #                            + str(position) + '.xlsx')
                #    available_lineload.to_excel(filename)
                filename = os.path.join(self.__result_path, 'Power_with_CP_at_' + str(position) + '.xlsx')
                power.to_excel(filename)
            else:
                #if position is not '0':
                #    filename = os.path.join(self.__result_path, 'Available_Lineload_with_storage_at_'
                #                            + str(position) + '.xlsx')
                #    available_lineload.to_excel(filename)
                filename = os.path.join(self.__result_path, 'Power_with_storage_at_' + str(position) + '.xlsx')
                power.to_excel(filename)
        else:
            power: pd.DataFrame = self.get_load_profile_at_position(edisgo, position)

            if not storage and not charging_parks:
                filename = os.path.join(self.__result_path, 'Power_at_' + str(position) + '.xlsx')
                power.to_excel(filename)
                if position.find('_') > -1:
                    filename = os.path.join(self.__result_path, 'Power_at_' + str(position[:position.find('_')]) + '.xlsx')
                    power: pd.DataFrame = self.get_load_profile_at_position(edisgo, position[:position.find('_')])
                    power.to_excel(filename)
            elif charging_parks and not storage:
                filename = os.path.join(self.__result_path, 'Power_with_CP_at_' + str(position) + '.xlsx')
                power.to_excel(filename)
                if position.find('_') > -1:
                    filename = os.path.join(self.__result_path, 'Power_with_CP_at_' + str(position[:position.find('_')]) + '.xlsx')
                    power: pd.DataFrame = self.get_load_profile_at_position(edisgo, position[:position.find('_')])
                    power.to_excel(filename)
            else:
                filename = os.path.join(self.__result_path, 'Power_with_storage_at_' + str(position) + '.xlsx')
                power.to_excel(filename)
                if position.find('_') > -1:
                    filename = os.path.join(self.__result_path, 'Power_with_storage_at_' + str(position[:position.find('_')]) + '.xlsx')
                    power: pd.DataFrame = self.get_load_profile_at_position(edisgo, position[:position.find('_')])
                    power.to_excel(filename)


    def transformer_power(self, edisgo: EDisGo, charging_parks: bool = False, storage: bool = False) -> None:
        '''
        Calculates the transformer power (PCC)

        Parameters
        ----------
        edisgo : EDisGo object
        charging_parks : charging parks included ?
        storage : BESS included ?
        Returns
        -------

        '''
        transformer_power = self.__grid_analysis.hv_transformer_power(edisgo)
        if not storage and not charging_parks:
            filename = os.path.join(self.__result_path, 'Transformer_power.xlsx')
            transformer_power.to_excel(filename)
            self.__transformer_energy_without_cp = transformer_power.sum()
        elif charging_parks and not storage:
            filename = os.path.join(self.__result_path, 'Transformer_power_with_CP.xlsx')
            transformer_power.to_excel(filename)
            self.__transformer_energy_with_cp = transformer_power.sum()
            emob_energy_share = (self.__transformer_energy_with_cp - self.__transformer_energy_without_cp) / \
                                 self.__transformer_energy_with_cp
            print('EMobilityShare: ' + str(emob_energy_share))
        else:
            filename = os.path.join(self.__result_path, 'Transformer_power_with_storage.xlsx')
            transformer_power.to_excel(filename)
            self.__transformer_energy_with_storage = transformer_power.sum()
            sizing = StorageSizing(transformer_power, timestep=300)

    def get_load_profile_at_position(self, edisgo: EDisGo, position: str) -> pd.DataFrame:
        '''

        Parameters
        ----------
        edisgo : EDisGo object
        position : position id

        Returns
        -------
            pandas datarame: Power in W

        '''
        storage = False

        if position == '0':
            power: pd.DataFrame = self.__grid_analysis.hv_transformer_power(edisgo)
            return power

        if self.__grid_config.mv_only or position.find('_') == -1:
            if self.__grid_config.mv_only:
                position = position + '_MV'
            elif position.find('_') == -1:
                position = 'Transformer_lv_grid_' + position[:]

            for name in edisgo.results.pfa_p:
                if position in name:
                    grid_name = name
                    break

            P = edisgo.results.pfa_p[grid_name]
            Q = edisgo.results.pfa_q[grid_name]

            power: pd.DataFrame = (P ** 2 + Q ** 2) ** 0.5 * 1e6 * np.sign(P) # MW to W
            return power

        else:
            position_edisgo = 'Load_mvgd_' + self.__grid_config.grid_id + '_lvgd_' + position
            P = edisgo.timeseries.loads_active_power[position_edisgo]
            Q = edisgo.timeseries.loads_reactive_power[position_edisgo]

            if edisgo.topology.storage_units_df.empty:
                P_storage = P * 0
                Q_storage = Q * 0
            else:
                for buses in edisgo.topology.storage_units_df.bus:
                    position = 'lvgd_' + position[:position.find('_')] + '_loa' + position[position.find('_'):]
                    if position in buses:
                        position_edisgo = edisgo.topology.storage_units_df.index[edisgo.topology.storage_units_df.bus == buses][0]
                        storage = True
                        break
                if storage:
                    P_storage = edisgo.timeseries.storage_units_active_power[position_edisgo]
                    Q_storage = edisgo.timeseries.storage_units_reactive_power[position_edisgo]
                else:
                    P_storage = P * 0
                    Q_storage = Q * 0

            power: pd.DataFrame = ((P - P_storage) ** 2 + (Q - Q_storage) ** 2) ** 0.5 * 1e6 * np.sign(P)  # MW to W
            return power