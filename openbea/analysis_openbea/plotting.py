import os
from configparser import ConfigParser
import pandas as pd
import matplotlib.pyplot as pyplt
import seaborn as sns

from openbea.config.simulation.general_config import GeneralConfig

class Plotting:
    def __init__(self, result_path: str = None, timeindex: pd.DatetimeIndex = None, simulation_config: ConfigParser = None):
        self._genaral_config = GeneralConfig(simulation_config)
        self._save_results = self._genaral_config.save_results
        self.__timeindex = timeindex
        self.__result_path = result_path

    def plot_power_at_0(self, power_before, power_after) -> None:
        fig, ax = pyplt.subplots(figsize=(20, 10))
        ax.plot(power_before, color='red', linestyle='solid', linewidth=0.5, zorder=0, label='Power before Storage')
        for idx, power_after_item in enumerate(power_after, start=1): #power_after delivered as list, so enumerate
            ax.plot(power_after_item, linewidth=0.5, zorder=10, label='Power after Storage '+str(idx))
        ax.set_xlabel("Time", fontsize=12)
        ax.set_ylabel("Power in kVA at HV/MV", fontsize=12)
        ax.legend()
        filename = os.path.join(self.__result_path, 'power_at_0')
        pyplt.savefig(filename)
        pyplt.close()
        pyplt.show()


    def plot_values(self, df, y_list: list, x: str, hue = "OPTIMIZATION", prefix = "", facet_col = "POSITION"): # creates matplot graphs and saves them
        tex_fonts = {
            # Use LaTeX to write all text
            "text.usetex": True,
            "font.family": "serif",
            # Use 10pt font in plots, to match 10pt font in document
            "axes.labelsize": 24,
            "font.size": 24,
            # Make the legend/label fonts a little smaller
            "legend.fontsize": 24,
            "xtick.labelsize": 24,
            "ytick.labelsize": 24,
            'figure.figsize': (11.7, 8.27)
        }

        try:
            for y in y_list:
                sns.barplot(x=x, y=y, data=df, palette="gist_gray_r", ci=None)
                pyplt.rcParams.update(tex_fonts)
                filename = os.path.join(self.__result_path, y+" Barplot.pdf")
                pyplt.legend(frameon=False)
                pyplt.tight_layout()
                pyplt.savefig(filename)
                pyplt.show()
                pyplt.clf()

        except Exception as e:
            print("--- Advanced Plotting failed ---")
            print(str(e))

    def plot_storage_timeseries(self, simses_power, edisgo_power, simses_soc, edisgo_soc,
                                load_profile, position: str) -> None:
        fig, ax = pyplt.subplots()
        if simses_power is not None:
            ax.plot(self.__timeindex, simses_power / 1e3, color='b', label='SimSES Power')
        ax.plot(self.__timeindex, edisgo_power / 1e3, color='r', linestyle=':', label='Power')
        ax.plot(self.__timeindex, load_profile / 1e3, color='g', linestyle='-.', label='Load Profile')
        ax.set_xlabel("Time", fontsize=12)
        ax.set_ylabel("Power in kVA", fontsize=12)
        ax2 = ax.twinx()
        if simses_soc is not None:
            ax2.plot(self.__timeindex, simses_soc, color='c', label='SimSES SOC')
        ax2.plot(self.__timeindex, edisgo_soc, color='m', linestyle=':', label='SOC')
        ax2.set_ylabel("SOC in p.u", fontsize=12)
        ax2.set_ylim(0, 1.05)
        ax.legend()
        ax2.legend()
        if self._save_results:
            if not os.path.isdir(os.path.join(self.__result_path, position)):
                os.mkdir(os.path.join(self.__result_path, position))
            filename = os.path.join(self.__result_path, position, 'storage_power')
            pyplt.savefig(filename)
            pyplt.close()
            pyplt.show()
        else:
            pyplt.show()
