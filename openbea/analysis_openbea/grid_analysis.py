import os
from configparser import ConfigParser
from math import sqrt, floor
import networkx as nx
import pandas as pd
import numpy as np
from edisgo import EDisGo
from edisgo.tools.plots import mv_grid_topology

from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.grid_config import GridConfig
from openbea.config.simulation.costs_config import CostsConfig
from openbeaconstants import CHARGING_PARK, LINOPT_CONSTRAINTS
from openbeaconstants import GRID_PATH, CACHE_PATH
import re
from openbea.config.simulation.general_config import GeneralConfig
import pickle
import xlsxwriter

class GridAnalysis:
    """
    Basic Grid Analysis
    """

    def __init__(self, result_path: str = None, simulation_config: ConfigParser = None):
        self._genaral_config = GeneralConfig(simulation_config)
        self._grid_config = GridConfig(simulation_config)
        self._costs_config = CostsConfig(simulation_config)
        self._save_results = self._genaral_config.save_results
        self._plot_each_timestep = self._genaral_config.plot_each_step
        self.__result_path = result_path
        self.__cache = CACHE_PATH

        # parameters for costs analysis
        self.__discount_rate = self._costs_config.discount_rate
        self.__depreciation_period_grid = self._costs_config.depreciation_period_grid
        self.__mv_only = self._grid_config.mv_only

        # setting constants for pickle identifier
        self.__genaral_config = GeneralConfig(simulation_config)
        self.__load_multiplier = self.__genaral_config.load_timeseries_multiplier
        self.__periods = floor(self.__genaral_config.duration / self.__genaral_config.timestep)
        self.__frequency = str(self.__genaral_config.timestep) + 'S'
        self.__start_time = self.__genaral_config.start
        self.__grid_config = GridConfig(simulation_config)
        self.__identifier = self._create_pickle_name_from_config()

    # function to create a pickle name based on the current config - as in create edisgo
    def _create_pickle_name_from_config(self) -> str:
        # collect unique identifiers which enable a differentiation between simulations
        identifiers = [self.__load_multiplier, self.__periods, self.__frequency, self.__start_time
                       ,self.__grid_config.grid_id]

        identification = "PFA"
        for identifier in identifiers:
            identification = identification + re.sub('\D', '', str(identifier))

        return identification

    def plot_example_grid(self, edisgo: EDisGo) -> None:
        if self._save_results:
            edisgo.plot_mv_grid_topology(
                filename=os.path.join(self.__result_path, 'Example_Grid.svg'),
                technologies=True, background_map=False, title='Example Grid')

            edisgo.plot_mv_line_loading(technologies=True, limits_cb_lines=[0, 1.1], background_map=False,
                                        title='Example Grid - Load Flow Analysis - Worst Case',
                                        filename=os.path.join(self.__result_path, 'Worst_Case.svg'))
            if self._plot_each_timestep:
                self._plot_each_timestep(edisgo, storage=False)


    def grid_load_analysis(self, edisgo: EDisGo) -> EDisGo:
        if self._grid_config.mv_only:
            edisgo.analyze(mode='mv')
        else:
            edisgo.analyze()
        return edisgo

    def create_pickle(self, edisgo: EDisGo) -> None:
        # try to pickle dataframe for reuse
        try:
            if (os.path.exists(self.__cache)):  # check if cache folder exists
                pass
            else:
                os.mkdir(self.__cache)  # of not create the corresponding folder
            filename = self._create_pickle_name_from_config()  # get the unique identifier
            outfile = open(os.path.join(self.__cache, filename), 'wb')
            pickle.dump(edisgo, outfile)
            outfile.close()
            print("     Successfully storing edisgo dataframe as pickle.")
        except Exception as e:
            print("     " + str(e))
            print("     Error at pickling edisgo dataframe.")
            pass


    def _plot_each_timestep(self, edisgo: EDisGo, storage: bool = False) -> None:
        timeindex = edisgo.timeseries.timeindex
        if storage:
            i = 0
            for timestep in timeindex:
                i += 1
                filename = os.path.join(self.__result_path, 'GridLoad_{}'.format(i))
                edisgo.plot_mv_line_loading(technologies=True, limits_cb_lines=[0, 1.1], background_map=False,
                                            title='Example Grid - Load Flow Analysis  w/ ESS - Step {}'.format(i),
                                            timestep=timestep, filename=filename)
        else:
            i = 0
            for timestep in timeindex:
                i += 1
                filename = os.path.join(self.__result_path, 'GridLoad_{}'.format(i))
                edisgo.plot_mv_line_loading(technologies=True, limits_cb_lines=[0, 1.1], background_map=False,
                                            title='Example Grid - Load Flow Analysis - Step {}'.format(i),
                                            timestep=timestep, filename=filename)


    def hv_transformer_power(self, edisgo: EDisGo) -> pd.DataFrame:
        '''

        Parameters
        ----------
        edisgo : EDisGo object

        Returns
        -------
            pandas datarame: Power in W

        '''
        apparent_power = (edisgo.results.hv_mv_exchanges.p**2 + edisgo.results.hv_mv_exchanges.q**2)**0.5
        return apparent_power * 1e6 * np.sign(edisgo.results.hv_mv_exchanges.p)

    def get_line_lv_load(self, edisgo: EDisGo, position: str) -> pd.DataFrame:
        '''

        Parameters
        ----------
        edisgo : EDisGo object
        position : Storage position

        Returns
        -------
            pandas datarame: Lineload in W for each step and line from hvmv substation to storage


        '''
        lineload = pd.DataFrame()
        if position == '0':
            return 0
        for names in edisgo.topology.buses_df.index:
            if names.endswith('MV') and position in names:
                position = names
                break

        grid = edisgo.topology.mv_grid
        graph = grid.graph
        path = nx.shortest_path(
            graph,
            grid.station.index[0],
            position)

        lineload = pd.DataFrame()
        for i in range(len(path[:-1])):
            line = grid.lines_df.loc[(grid.lines_df.bus0 == path[i]) & (
                    grid.lines_df.bus1 == path[i + 1])].append(
                grid.lines_df.loc[(grid.lines_df.bus1 == path[i]) & (
                        grid.lines_df.bus0 == path[i + 1])])
            i_res = edisgo.results.i_res[line.index]
            lineload = pd.concat([lineload, i_res], axis=1)

        line_indices = lineload.columns

        # get lines
        lines = edisgo.topology.lines_df.loc[line_indices]
        lines = lines.join(
            edisgo.topology.buses_df.loc[lines.bus0, "v_nom"],
            on="bus0",
            how="left",
        ).drop_duplicates()

        return lineload * lines.v_nom * sqrt(3) * 1e6

    def calculate_available_line_load(self, edisgo, line_load):
        """
        Calculates available line load.

        Line loading is calculated by dividing the current at the given time step
        by the allowed current.


        Parameters
        ----------
        edisgo : :class:`~.edisgo.EDisGo`
            Pypsa network with lines to calculate line loading for.
        line_load : :pandas:`pandas.DataFrame<DataFrame>`
            Dataframe with current results from power flow analysis in A. Index of
            the dataframe is a :pandas:`pandas.DatetimeIndex<DatetimeIndex>`,
            columns are the line representatives.
        lines : list(str) or None, optional
            Line names/representatives of lines to calculate line loading for. If
            None line loading of all lines in `line_load` dataframe are used.
            Default: None.
        timesteps : :pandas:`pandas.Timestamp<Timestamp>` or list(:pandas:`pandas.Timestamp<Timestamp>`) or None, optional
            Specifies time steps to calculate line loading for. If timesteps is
            None all time steps in `line_load` dataframe are used. Default: None.

        Returns
        --------
        :pandas:`pandas.DataFrame<DataFrame>`
            Dataframe with availaible line power for each step (W). Index of
            the dataframe is a :pandas:`pandas.DatetimeIndex<DatetimeIndex>`,
            columns are the line representatives.

        """
        timesteps = line_load.index
        line_indices = line_load.columns

        # current from power flow
        line_power = line_load.loc[timesteps, line_indices]
        # allowed current
        lines = edisgo.topology.lines_df.loc[line_indices]
        lines = lines.join(
            edisgo.topology.buses_df.loc[lines.bus0, "v_nom"],
            on="bus0",
            how="left",
        ).drop_duplicates()
        max_power = (0.5 * lines.s_nom * 1e6).to_frame("i_nom").T
        difference = max_power.values - line_power
        return difference.min(axis=1)

    def storage_influence_analysis(self, edisgo: EDisGo) -> None:
        if self._save_results:
            edisgo.plot_mv_line_loading(technologies=True, limits_cb_lines=[0, 1.1], background_map=False,
                                        title='Example Grid - Load Flow Analysis - Worst Case w/ ESS',
                                        filename=os.path.join(self.__result_path, 'Worst_Case_ESS.svg'))

            try:
                edisgo.plot_mv_storage_integration(filename=self.__result_path + '/Storage Integration.svg',
                                                   background_map=False)
            except:
                print('No active storage has been integrated')

    def plot_charging_parks(self, edisgo: EDisGo) -> None:
        if self._save_results:
            mv_grid_topology(edisgo_obj=edisgo, background_map=False, title='Charging Parks',
                             node_color=CHARGING_PARK, filename=os.path.join(self.__result_path, 'CP_Locations.svg'))
            edisgo.plot_mv_line_loading(technologies=True, limits_cb_lines=[0, 1.1], background_map=False,
                                        title='Example Grid - Load Flow Analysis - Worst Case with CP',
                                        filename=os.path.join(self.__result_path, 'Worst_Case_CP.svg'))

    def prepare_lin_opt(self, edisgo: EDisGo) -> None:
        LinOptDir = os.path.join(self.__result_path, LINOPT_CONSTRAINTS)
        line_constraints = edisgo.results.i_res
        line_constraints.to_csv(os.path.join(LinOptDir, 'Line_power.csv'), sep=',')


    # Save grid reinforcement costs as .xlsx-file
    def save_grid_reinforce_costs(self, edisgo: EDisGo, filename):
        self._grid_reinforce_total_costs = {}
        self._grid_reinforce_discounted_costs = {}
        self._grid_reinforce_specific_costs = {}

        if self.__mv_only:
            reinforce = edisgo.reinforce(copy_graph=True,mode='mv')  # reinforce grid (copy_graph = true; grid does not change; mode = 'mv'; grid reinforce only on mv-level)
        else:
            reinforce = edisgo.reinforce(copy_graph=True)  # reinforce grid (copy_graph = true; grid does not change)

        quantity = reinforce.grid_expansion_costs.quantity.to_dict()
        length = reinforce.grid_expansion_costs.length.to_dict()
        grid_reinforce_costs = reinforce.grid_expansion_costs.total_costs.to_dict()
        filename_costs = (os.path.join(self.__result_path, filename))

        for i in grid_reinforce_costs.keys():

            self._grid_reinforce_total_costs[i] = grid_reinforce_costs[i]*1000*quantity[i]
            self._grid_reinforce_discounted_costs[i] = (grid_reinforce_costs[i]*1000*quantity[i]) / (1 + self.__discount_rate)**self.__depreciation_period_grid

        with xlsxwriter.Workbook(filename_costs) as workbook:
            worksheet = workbook.add_worksheet()
            worksheet.write(0,0,'Equipment changes')
            worksheet.write(0,1,'Costs in $')
            worksheet.write(0,2,'Costs (discounted) in $')
            worksheet.write(0,4,'Length in km')
            worksheet.write(0,5,'Length per cable in km')

            for i, (k, v) in enumerate(self._grid_reinforce_total_costs.items(), start=1):
                worksheet.write(i, 0, k)
                worksheet.write(i, 1, v)

            for i, (k, v) in enumerate(self._grid_reinforce_discounted_costs.items(), start=1):
                worksheet.write(i, 2, v)

            for i, (k,v) in enumerate(length.items(), start=1):
                worksheet.write(i, 4, v)

            for i in grid_reinforce_costs.keys():
                if length[i] != 0:
                    length[i] = length[i]/quantity[i]
                    self._grid_reinforce_specific_costs[i] = grid_reinforce_costs[i]*1000/length[i]
                    worksheet.write(0, 3, 'Specific costs (cable) in $/km')
                    for i, (k, v) in enumerate(self._grid_reinforce_specific_costs.items(), start=1):
                        worksheet.write(i, 3, v)

            for i, (k,v) in enumerate(length.items(), start=1):
                worksheet.write(i, 5, v)


            worksheet.write(len(grid_reinforce_costs)+1,0,'All')
            worksheet.write(len(grid_reinforce_costs)+1,1,sum(self._grid_reinforce_total_costs.values()))
            worksheet.write(len(grid_reinforce_costs)+1,2,sum(self._grid_reinforce_discounted_costs.values()))