import pandas as pd
import xlsxwriter
import os

class SimulationAnalysis:

    def __init__(self, openBea, analysis_positions, edisgo, load_analysis, grid_analysis,timestep=900, result_path:str = None):
        self.openBea = openBea
        self.analysis_positions = analysis_positions
        self.edisgo = edisgo
        self.load_analysis = load_analysis
        self.grid_analysis = grid_analysis
        self.timestep = timestep
        self.__result_path = result_path

    def set_limit(self, limit):
        self.limit = limit

    def initial_calculations(self):
        # load power profile at analysis position, save it for result retrieval
        self.__power_initial = self.openBea.get_power(position=self.analysis_positions[0], edisgo=self.edisgo,
                                              grid_analysis=self.grid_analysis, load_analysis=self.load_analysis)

        self.__all_positions = ["0"] + self.analysis_positions  # create a list of all positions with 0 positions first
        # load power profiles at all analysis positions
        self.__powers_initial = [pd.DataFrame(self.openBea.get_power(position=analysis_position, edisgo=self.edisgo,
                                                             grid_analysis=self.grid_analysis, load_analysis=self.load_analysis))
                                 for
                                 analysis_position in self.__all_positions]


        self.__max_powers = [powerdf[powerdf.columns[0]].max() for powerdf in self.__powers_initial]
        self.__total_energy = [powerdf[powerdf.columns[0]].sum()*(self.timestep/3600) for powerdf in self.__powers_initial]

        # load power profile at '0' hvmv transformer for result retrieval
        self.__max_power_at_0_init = self.openBea.get_power(position='0', edisgo=self.edisgo,
                                                        grid_analysis=self.grid_analysis, load_analysis=self.load_analysis)

        self.__all_positions_lvmv = self.remove_sub(self.__all_positions.copy())


        self.__max_powers_lvmv_init = [pd.DataFrame(self.openBea.get_power(position=analysis_position, edisgo=self.edisgo,
                                                             grid_analysis=self.grid_analysis, load_analysis=self.load_analysis))
                                 for
                                 analysis_position in self.__all_positions_lvmv]


        self.__max_powers_lvmv = [powerdf[powerdf.columns[0]].max() for powerdf in self.__max_powers_lvmv_init]

    def remove_sub(self, positions):
        new_positions_lvmv = []
        for position in positions:
            if position.find('_') > -1:
                new_positions_lvmv.append(position[:position.find('_')])
            else:
                new_positions_lvmv.append(position)
        return new_positions_lvmv


    def storage_integration_analysis(self):

        self.__power_after_storage = self.openBea.get_power(position=self.analysis_positions[0], edisgo=self.edisgo,
                                                    grid_analysis=self.grid_analysis, load_analysis=self.load_analysis)

        self.__powers_after_storage = [pd.DataFrame(self.openBea.get_power(position=analysis_position, edisgo=self.edisgo,
                                                                   grid_analysis=self.grid_analysis,
                                                                   load_analysis=self.load_analysis)) for
                                       analysis_position in self.__all_positions]

        self.__powers_after_storage_lvmv = [pd.DataFrame(self.openBea.get_power(position=analysis_position, edisgo=self.edisgo,
                                                                   grid_analysis=self.grid_analysis,
                                                                   load_analysis=self.load_analysis)) for
                                       analysis_position in self.__all_positions_lvmv]
        self.__max_powers_after_storage = [powerdf[powerdf.columns[0]].max() for powerdf in self.__powers_after_storage]
        self.__max_powers_after_storage_lvmv = [powerdf[powerdf.columns[0]].max() for powerdf in self.__powers_after_storage_lvmv]
        self.__total_energy_after = [powerdf[powerdf.columns[0]].sum() * (self.timestep / 3600) for powerdf in
                               self.__powers_after_storage]

    def final_analysis(self, storage_simulation, storage_capacity):
        # load power profile at '0' hvmv transformer for result retrieval
        self.__max_power_at_0_final = self.openBea.get_power(position='0', edisgo=self.edisgo,
                                                     grid_analysis=self.grid_analysis, load_analysis=self.load_analysis)
        self.__storage_capacity = storage_capacity
        invest_costs = storage_simulation.get_invest_storage_costs()
        discounted_costs = storage_simulation.get_discounted_storage_costs()
        specific_costs = storage_simulation.get_specific_storage_costs()
        filename=(os.path.join(self.__result_path,'Storage_costs.xlsx'))

        # Save storage_costs in .xlsx-file
        with xlsxwriter.Workbook(filename) as workbook:
            worksheet = workbook.add_worksheet()
            worksheet.write(0,0,'Position')
            worksheet.write(0,1,'Capacity in Wh')
            worksheet.write(0,2,'Investment Costs in $')
            worksheet.write(0,3, 'Investment Costs (discounted) in $')

            for i, capacity in enumerate(self.__storage_capacity):
                worksheet.write(i+1,1,self.__storage_capacity[i]/3600)

            for i, (k, v) in enumerate(invest_costs.items(), start=1):
                worksheet.write(i, 0, k)
                worksheet.write(i, 2, v)

            for i, (k, v) in enumerate(discounted_costs.items(), start=1):
                worksheet.write(i, 3, v)
                if len(specific_costs) == 1:
                    worksheet.write(0,4,'Specific energy investment costs (system) in $/kWh')
                    worksheet.write(i, 4, specific_costs[0])
                elif len(specific_costs) == 2:
                    worksheet.write(0,4,'Specific energy investment costs (storage) in $/kWh')
                    worksheet.write(i, 4, specific_costs[0])
                    worksheet.write(0, 5, 'Specific power investment costs in $/kW')
                    worksheet.write(i, 5, specific_costs[1])

            worksheet.write(len(invest_costs)+1,0,'All')
            worksheet.write(len(invest_costs)+1,2,sum(invest_costs.values()))
            worksheet.write(len(invest_costs)+1,3,sum(discounted_costs.values()))

    def save_storage_avoid_grid_reinforcement(self,optimal_capacity,equipment_changes):
        filename = (os.path.join(self.__result_path, 'Storage_for_Grid_Reinforcement.xlsx'))
        with xlsxwriter.Workbook(filename) as workbook:
            worksheet = workbook.add_worksheet()
            worksheet.write(0, 0, 'Position')
            worksheet.write(0, 1, 'Optimal Capacity in Wh')

            for i,capacity in enumerate(optimal_capacity):
                worksheet.write(i+1,0,equipment_changes[i])
                worksheet.write(i+1,1,optimal_capacity[i])


    def get_results(self):
        results = {'positions':self.__all_positions, 'load_sum_before':self.__total_energy,'load_sum_after':self.__total_energy_after, 'max_powers_after_storage':self.__max_powers_after_storage,
                   'max_powers_after_storage_lvmv':self.__max_powers_after_storage_lvmv,
                   'max_powers':self.__max_powers, 'max_powers_lvmv':self.__max_powers_lvmv, 'max_power_at_0_init':self.__max_power_at_0_init,
                   'max_power_at_0_final':self.__max_power_at_0_final,'storage_capacity':self.__storage_capacity, 'limit':self.limit}


        return results
