import warnings
warnings.filterwarnings("ignore")
from openbea.analysis_openbea.plotting import Plotting
from openbea.config.simulation.storage_config import StorageConfig
from openbea.add_timeseries.emobility import EMobility
from openbea.analysis_openbea.load_analysis import LoadAnalysis
from openbea.config.simulation.charging_parks_config import ChargingParkConfig
from openbea.config.simulation.general_config import GeneralConfig
from openbea.config.simulation.grid_config import GridConfig
from openbea.storage_integration.storage_simulation import StorageSimulation
from configparser import ConfigParser
from openbea.grids.create_edisgo import CreateEdisgo
from openbea.analysis_openbea.grid_analysis import GridAnalysis
from openbea.analysis_openbea.simulation_analysis import SimulationAnalysis
from openbea.storage_integration.storage_reinforce_grid import StorageReinforceGrid
import logging
loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
for logger in loggers:
    logger.setLevel(logging.ERROR)

# imports for reference series
from timeit import default_timer as timer
from openbeaconstants import RESULT_PATH
import os
import pandas as pd
import math
from openbea import settings



class OpenBEA:
    def __init__(self, multiple_simulations: bool = False, simualtion_id: str = None, multiple_simulations_name: str = None):
        if not multiple_simulations and simualtion_id == None:
            start_time = '{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}'.format(pd.datetime.now().year, pd.datetime.now().month,
                                                                       pd.datetime.now().day, pd.datetime.now().hour,
                                                                       pd.datetime.now().minute,
                                                                       pd.datetime.now().second)
            self.__result_path = os.path.join(RESULT_PATH, start_time)
        elif not multiple_simulations:
            self.__result_path = os.path.join(RESULT_PATH, simualtion_id)
        else:
            if simualtion_id == None or multiple_simulations_name == None:
                raise Exception('simulation_id necessary for multiple simulations')
            multi_path = os.path.join(RESULT_PATH, multiple_simulations_name)
            if not os.path.isdir(multi_path):
                os.mkdir(multi_path)
            self.__result_path = os.path.join(multi_path, simualtion_id)
        if os.path.isdir(self.__result_path):
            raise Exception('multiple_simulations_name + id already exists')
        os.mkdir(self.__result_path)

    def start(self, simulation_config: ConfigParser = None):
        self.__start_time = timer()  # startpoint for runtime calculation
        self.__simulation_config = simulation_config # save variable for result calculation

        print('Start open_BEA')
        '''Config Settings'''
        charging_config = ChargingParkConfig(simulation_config)
        storage_config = StorageConfig(simulation_config)
        general_config = GeneralConfig(simulation_config)
        charging_positions = charging_config.position if general_config.charging_park_integration else []
        storage_positions = storage_config.position if general_config.storage_integration else []
        analysis_positions = charging_positions + storage_positions
        analysis_positions = list(set(analysis_positions))
        self.__analysis_positions = analysis_positions # save variable for result calculation
        settings.init()

        '''Create GridModel and necessary instances'''
        create_edisgo = CreateEdisgo(simulation_config)
        grid_analysis = GridAnalysis(result_path = self.__result_path, simulation_config=simulation_config)
        load_analysis = LoadAnalysis(result_path = self.__result_path, simulation_config=simulation_config)
        e_mobility = EMobility(result_path = self.__result_path, simulation_config=simulation_config)
        edisgo = create_edisgo.create_edisgo()

        '''Initial PFA'''
        print('Start Initial PFA')
        if create_edisgo.pickle_exists() == False and general_config.various_timeseries:
            edisgo = grid_analysis.grid_load_analysis(edisgo)
            grid_analysis.create_pickle(edisgo)
        elif not general_config.various_timeseries:
            edisgo = grid_analysis.grid_load_analysis(edisgo)
        grid_analysis.plot_example_grid(edisgo)
        load_analysis.transformer_power(edisgo=edisgo, charging_parks=False, storage=False)
        for position in analysis_positions:
            load_analysis.analysis(edisgo=edisgo, position=position, charging_parks=False, storage=False)
        print('     Initial PFA finished')

        '''Add Charging Stations'''
        if general_config.charging_park_integration:
            print('Adding charging parks')
            for position in charging_positions:
                edisgo = e_mobility.add_component(edisgo, position)
            print('Start PFA after charging park integration')
            edisgo = grid_analysis.grid_load_analysis(edisgo)
            load_analysis.transformer_power(edisgo=edisgo, charging_parks=True, storage=False)
            for position in analysis_positions:
                load_analysis.analysis(edisgo=edisgo, position=position, charging_parks=True, storage=False)
            grid_analysis.plot_charging_parks(edisgo)
            print('     PFA after charging park integration finished')

        '''Check Reinforce'''
        if general_config.grid_reinforce:
            grid_analysis.save_grid_reinforce_costs(edisgo = edisgo, filename = 'Grid_reinforce_costs_withoutEES.xlsx')

        if general_config.storage_integration:
            '''Check AVOID GRID REINFORCEMENT BY STORAGE INTEGRATION'''
            if storage_config.avoid_grid_reinforcement: # storage capacity is calculated by iteration algorithm
                print('Iteration of storage capacity')
                storage_reinforce_grid = StorageReinforceGrid()
                # Get Position ID's of equipment changes without storage integration
                equipment_changes = storage_reinforce_grid.extract_grid_reinforcement_positions(edisgo=edisgo)
                print('Changes in grid equipment:',equipment_changes)
                simulation_analysis = SimulationAnalysis(openBea=self, analysis_positions=self.__analysis_positions,
                                                             edisgo=edisgo,
                                                             load_analysis=load_analysis, grid_analysis=grid_analysis,
                                                             timestep=general_config.timestep,
                                                             result_path=self.__result_path)

                simulation_analysis.initial_calculations()

                '''Integrate Storage Units'''
                # Iterate the storage capacity while the grid is reinforced at the certain position
                optimal_capacity = [0]*len(equipment_changes)
                for i,changes in enumerate(equipment_changes):
                    settings.avoided_equipment = [equipment_changes[i]]
                    print('Avoided equipment',settings.avoided_equipment)
                    iteration_start = True
                    while iteration_start:
                        edisgo = create_edisgo.create_edisgo()
                        edisgo = grid_analysis.grid_load_analysis(edisgo)

                        storage_simulation = StorageSimulation(result_path=self.__result_path,
                                                           simulation_config=simulation_config, edisgo=edisgo)
                        edisgo = storage_simulation.start()

                        print('Start PFA after storage integration')
                        edisgo = grid_analysis.grid_load_analysis(edisgo)
                        load_analysis.transformer_power(edisgo=edisgo, charging_parks=False, storage=True)
                        for position in analysis_positions:
                            load_analysis.analysis(edisgo=edisgo, position=position, charging_parks=False, storage=True)
                        grid_analysis.storage_influence_analysis(edisgo)
                        print('     PFA after storage integration finished')
                        simulation_analysis.storage_integration_analysis()  # Calculate simulation results of storage integration
                        simulation_analysis.set_limit(storage_simulation.get_peak_shaving_limit_adapted())

                        simulation_analysis.final_analysis(storage_simulation,[settings.actual_capacity])
                        self.__results = simulation_analysis.get_results()
                        plotting = Plotting(result_path=self.__result_path)
                        plotting.plot_power_at_0(power_before=self.__results.get('max_power_at_0_init'),
                                             power_after=[self.__results.get('max_power_at_0_final')])
                         # Get Position ID's of equipment changes after storage integration
                        settings.equipment_changes_after_storage = storage_reinforce_grid.extract_grid_reinforcement_positions(edisgo)

                        settings.actual_capacity = storage_simulation.get_storage_capacity() * 3600
                        print('Actual Capacity in Iteration Step',settings.actual_capacity/3600,' Wh')

                        if settings.avoided_equipment[0] in settings.equipment_changes_after_storage and abs(optimal_capacity[i] - settings.actual_capacity) < 100000.0 * 3600 and settings.actual_capacity > 100000*3600:
                            settings.actual_capacity = settings.actual_capacity / 3600
                            optimal_capacity[i] = self.roundup(settings.actual_capacity)
                            iteration_start = False
                        elif settings.avoided_equipment[0] not in settings.equipment_changes_after_storage and settings.actual_capacity > 100000*3600:
                            optimal_capacity[i] = settings.actual_capacity
                            iteration_start = True
                        elif settings.avoided_equipment[0] not in settings.equipment_changes_after_storage and settings.actual_capacity < 100000*3600:
                            settings.actual_capacity = settings.actual_capacity / 3600
                            optimal_capacity[i] = self.roundup(settings.actual_capacity)
                            iteration_start = False
                        elif settings.avoided_equipment[0] in settings.equipment_changes_after_storage and settings.actual_capacity > 1999000*3600:
                            optimal_capacity[i] = '> 2000'
                            iteration_start = False
                        else:
                            iteration_start = True

                        settings.iteration_step += 1

                    settings.actual_capacity = 0
                    settings.equipment_changes_after_storage = []
                    settings.iteration_step = 1
                    print('Optimal Capacity: ',optimal_capacity,'in Wh')
                print('     Iteration of storage capacity finished')
                simulation_analysis.save_storage_avoid_grid_reinforcement(optimal_capacity,equipment_changes)

            else:
                '''Initiate SimulationAnalysis to store results and calculate metrics'''
                simulation_analysis = SimulationAnalysis(openBea=self,analysis_positions=self.__analysis_positions,edisgo=edisgo,
                                                     load_analysis=load_analysis,grid_analysis=grid_analysis,
                                                     timestep=general_config.timestep,result_path = self.__result_path)
                simulation_analysis.initial_calculations()

                '''Integrate Storage Units'''

                storage_simulation = StorageSimulation(result_path=self.__result_path, simulation_config=simulation_config, edisgo=edisgo)
                edisgo = storage_simulation.start()

                print('Start PFA after storage integration')
                edisgo = grid_analysis.grid_load_analysis(edisgo)
                load_analysis.transformer_power(edisgo=edisgo, charging_parks=False, storage=True)
                for position in analysis_positions:
                    load_analysis.analysis(edisgo=edisgo, position=position, charging_parks=False, storage=True)
                grid_analysis.storage_influence_analysis(edisgo)
                print('     PFA after storage integration finished')
                simulation_analysis.storage_integration_analysis() # Calculate simulation results of storage integration
                simulation_analysis.set_limit(storage_simulation.get_peak_shaving_limit_adapted())

                simulation_analysis.final_analysis(storage_simulation,storage_config.capacity)
                self.__results = simulation_analysis.get_results()
                plotting = Plotting(result_path=self.__result_path)
                plotting.plot_power_at_0(power_before=self.__results.get('max_power_at_0_init'), power_after=[self.__results.get('max_power_at_0_final')])

        '''Check Reinforce after Storage Integration'''
        if general_config.grid_reinforce and general_config.storage_integration and storage_config.avoid_grid_reinforcement == False: # Save the grid reinforcement costs which are needed after storage integrations
            grid_analysis.save_grid_reinforce_costs(edisgo = edisgo, filename = 'Grid_reinforce_costs.xlsx')

        print('openBEA finished')

    # given storage position, edisgo, grid_analysis and load_analysis, gets power timeseries
    def get_power(self, position, edisgo, grid_analysis, load_analysis):
        if position == "0":  # get transformer power for analysis either from hv mv node or storage position
            power = grid_analysis.hv_transformer_power(edisgo)
        else:
            power = load_analysis.get_load_profile_at_position(edisgo, position=position)
        return power

    # returns simulation powers as well as before and after values in dict
    def get_simulations_results(self):
        return self.__results

    def roundup(self,number:float):
        return math.ceil(number/100000.0) * 100000.0

if __name__ == "__main__":
    # minimum working example
    # config: ConfigParser = ConfigParser()
    # config.add_section('GENERAL')
    # config.set('GENERAL', 'TIME_STEP', '60')
    open_bea: OpenBEA = OpenBEA()
    open_bea.start()