from configparser import ConfigParser
from openbea.config.simulation.simulation_config import SimulationConfig
from openbeaconstants import TRUTH_CHECKER


class GeneralConfig(SimulationConfig):

    def __init__(self, config: ConfigParser, path: str = None):
        super().__init__(path, config)
        self.__section: str = 'GENERAL'

    @property
    def timestep(self) -> float:
        """Returns simulation timestep in s"""
        return float(self.get_property(self.__section, 'TIME_STEP'))

    @property
    def start(self) -> str:
        """Returns simulation start timestamp"""
        return str(self.get_property(self.__section, 'START'))

    @property
    def duration(self) -> float:
        """Returns simulation duration in s"""
        return float(self.get_property(self.__section, 'DURATION'))

    @property
    def save_results(self) -> bool:
        """Returns a bool if you want to save the results"""
        return self.get_property(self.__section, 'SAVE_RESULTS').lower() in TRUTH_CHECKER

    @property
    def plot_each_step(self) -> bool:
        """Returns a bool if you want to plot each step"""
        return self.get_property(self.__section, 'PLOT_EACH_STEP').lower() in TRUTH_CHECKER

    @property
    def storage_integration(self) -> bool:
        """Returns a bool if you want to integrate a storage"""
        return self.get_property(self.__section, 'STORAGE').lower() in TRUTH_CHECKER

    @property
    def charging_park_integration(self) -> bool:
        """Returns a bool if you want to integrate a charging park"""
        return self.get_property(self.__section, 'CHARGING_PARKS').lower() in TRUTH_CHECKER

    @property
    def storage_mode(self) -> str:
        """Returns a str with the storage mode you want (e.g. PeakShaving or linopt)"""
        return str(self.get_property(self.__section, 'STORAGE_MODE'))

    @property
    def simses_validation(self) -> bool:
        """Returns a bool if you want to integrate a storage in open_BEA and validate it with SimSES"""
        return self.get_property(self.__section, 'SIMSES_VALIDATION').lower() in TRUTH_CHECKER

    @property
    def load_timeseries_multiplier(self) -> float:
        """Returns multiplier for all intitial timeseries"""
        return float(self.get_property(self.__section, 'LOAD_TIMESERIES_MULTIPLIER'))

    @property
    def various_timeseries(self) -> bool:
        """Returns a bool if you want to have various timeseiers for each consumer"""
        return self.get_property(self.__section, 'VARIOUS_TIMESERIES').lower() in TRUTH_CHECKER

    @property
    def grid_reinforce(self) -> bool:
        """Returns a bool if you want to check if the grid has to reinforce"""
        return self.get_property(self.__section, 'GRID_REINFORCE').lower() in TRUTH_CHECKER
