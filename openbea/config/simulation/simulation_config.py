from configparser import ConfigParser
from openbea.config.config import Config


class SimulationConfig(Config):

    config_name: str = 'simulation'

    def __init__(self, path: str, config: ConfigParser):
        super().__init__(path, self.config_name, config)


def create_dict_from(properties: [str]) -> dict:
    res: dict = dict()
    for prop in properties:
        items: list = prop.split(',')
        name: str = items.pop(0)
        if name in res.keys():
            raise Exception(name + ' is not unique. Please check your config file.')
        res[name] = items
    return res

def create_list_from(prop: str) -> [str]:
    return prop.split(', ')