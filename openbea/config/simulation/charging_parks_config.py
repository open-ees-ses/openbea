from configparser import ConfigParser

from openbea.config.simulation.simulation_config import SimulationConfig, create_list_from
from openbeaconstants import TRUTH_CHECKER


class ChargingParkConfig(SimulationConfig):

    def __init__(self, config: ConfigParser, path: str = None):
        super().__init__(path, config)
        self.__section: str = 'CHARGING_PARKS'

    @property
    def position(self) -> [str]:
        """Returns a list of charging_park positions"""
        props: str = self.get_property(self.__section, 'POSITION')
        return create_list_from(props)

    @property
    def freq(self) -> float:
        """Returns charging_park timeseies frequency"""
        return float(self.get_property(self.__section, 'FREQ'))

    @property
    def peak_load(self) -> float:
        """Returns charging_park peak_load"""
        return float(self.get_property(self.__section, 'PEAK_LOAD'))

    @property
    def charging_tech(self) -> [str]:
        """Returns charging_park operation mode"""
        return self.get_property(self.__section, 'CHARGING_TECH')
