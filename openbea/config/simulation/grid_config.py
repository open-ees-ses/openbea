from configparser import ConfigParser

from openbea.config.simulation.simulation_config import SimulationConfig
from openbeaconstants import TRUTH_CHECKER


class GridConfig(SimulationConfig):

    def __init__(self, config: ConfigParser, path: str = None):
        super().__init__(path, config)
        self.__section: str = 'GRID'

    @property
    def grid_id(self) -> str:
        """Returns the grid ids"""
        return self.get_property(self.__section, 'GRID_ID')

    @property
    def mv_only(self) -> bool:
        """Returns the mode"""
        return self.get_property(self.__section, 'MV_ONLY').lower() in TRUTH_CHECKER

    @property
    def cos_phi(self) -> float:
        """Returns the grid ids"""
        return float(self.get_property(self.__section, 'COS_PHI'))