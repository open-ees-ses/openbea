from configparser import ConfigParser
from openbea.config.simulation.simulation_config import SimulationConfig, create_list_from
from openbeaconstants import TRUTH_CHECKER

class StorageConfig(SimulationConfig):

    def __init__(self, config: ConfigParser, path: str = None):
        super().__init__(path, config)
        self.__section: str = 'STORAGE'

    @property
    def position(self) -> [[str]]:
        """Returns a list of charging_park positions"""
        props: str = self.get_property(self.__section, 'POSITION')
        return create_list_from(props)

    @property
    def soc(self) -> float:
        """
        Minimum SOC (0-1)

        Returns
        -------
        float:
            Returns the start soc from config

        """
        return float(self.get_property(self.__section, 'START_SOC'))

    @property
    def min_soc(self) -> float:
        """
        Minimum SOC (0-1)

        Returns
        -------
        float:
            Returns the minimum soc from config

        """
        return float(self.get_property(self.__section, 'MIN_SOC'))

    @property
    def max_soc(self) -> float:
        """
        Maximum SOC (0-1)

        Returns
        -------
        float:
            Returns the maximum soc from config

        """
        return float(self.get_property(self.__section, 'MAX_SOC'))

    @property
    def efficiency(self) -> float:
        """
        Efficiency (0-1)

        Returns
        -------
        float:
            Returns the storage efficiency from config

        """
        return float(self.get_property(self.__section, 'EFFICIENCY'))

    @property
    def capacity(self) -> [[str]]:
        """
        Storage capacity in Ws

        Returns
        -------
        float:
            Returns a list of storage capacities in Ws from config

        """

        props: str = self.get_property(self.__section, 'CAPACITY')
        capacity_list =  create_list_from(props)
        capacity_list_float = [float(capacity)*3600 for capacity in capacity_list] # transform to floats, multiply by 3600 for correct units
        return capacity_list_float

    @property
    def peak_shaving_limit(self) -> [[str]]:
        """
        PS limit (0-1)

        Returns
        -------
        float:
            Returns a list of the peak shaving limits from config

        """
        props: str = self.get_property(self.__section, 'PEAK_SHAVING_LIMIT')

        return [float(limit) for limit in create_list_from(props)]


    @property
    def pe_ratio_ch(self) -> float:
        """
        PE ratio charging

        Returns
        -------
        float:
            Returns the max charge rate

        """
        return float(self.get_property(self.__section, 'PE_RATIO_CH'))

    @property
    def pe_ratio_dch(self) -> float:
        """
        PE ratio discharge

        Returns
        -------
        float:
            Returns max discharge rate
        """
        return float(self.get_property(self.__section, 'PE_RATIO_DCH'))

    @property
    def cell_type(self) -> str:
        """
        cell type (e.g. LFP)

        Returns
        -------
        float:
            Returns the cell type from config

        """
        return self.get_property(self.__section, 'CELL_TYPE')

    @property
    def fixed_storage(self) -> bool:
        """
        Storage config. if true, storage config is fixed, otherwise openBEA calculates the storage size itselfs

        Returns
        -------
        float:
            Returns the maximum soc from data_config file

        """
        return self.get_property(self.__section, 'FIXED_STORAGE').lower() in TRUTH_CHECKER

    @property
    def optimization_algorithm(self) -> str:
        """
        Optimization algorithm, used in Storage Sizing

        Returns
        -------
        float:
            Returns the cell type from config

        """
        return self.get_property(self.__section, 'OPTIMIZATION')

    @property
    def grid_centered(self) -> bool:
        """
        If storage should be optimized for specific storage location or whole grid ('0' transformer, hvmv)
        Returns
        -------
        Bool:
        True if Grid-Centered

        """
        return self.get_property(self.__section, 'GRID_CENTERED').lower() in TRUTH_CHECKER

    @property
    def grid_centered_ancilliary(self) -> bool:
        """
        If grid-centered peak shaving is only focus or ancilliary service (additional to local peak shaving)
        -------
        Bool:
        True if Grid-Centered ancilliary

        """
        return self.get_property(self.__section, 'GRID_CENTERED_ANCILLIARY').lower() in TRUTH_CHECKER

    def high_load_frames(self) -> bool:
        """
        Storage config. if true, high load frames are enabled,

        Returns
        -------
        Bool:
        True if HL Time frames are enabled

        """
        return self.get_property(self.__section, 'HIGH_LOAD_FRAMES').lower() in TRUTH_CHECKER

    @property
    def avoid_grid_reinforcement(self) -> bool:
        """
        Storage config. if true, storage is dimensioned to avoid grid reinforcement

        Returns
        -------
        Bool:
        True if storage capacity is calculated by iteration algorithm

        """
        return self.get_property(self.__section, 'AVOID_GRID_REINFORCEMENT').lower() in TRUTH_CHECKER
