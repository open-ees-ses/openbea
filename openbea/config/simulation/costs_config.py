from configparser import ConfigParser
from openbea.config.simulation.simulation_config import SimulationConfig
from openbeaconstants import TRUTH_CHECKER


class CostsConfig(SimulationConfig):

    def __init__(self, config: ConfigParser, path: str = None):
        super().__init__(path, config)
        self.__section: str = 'COSTS'

    @property
    def specific_investment_costs_energy_system(self) -> float:
        """Returns specific_investment_costs for energy_storage_system"""
        return float(self.get_property(self.__section, 'SPECIFIC_INVESTMENT_COSTS_ENERGY_SYSTEM'))

    @property
    def specific_investment_costs_energy_storage(self) -> float:
        """Returns specific_investment_costs for energy_storage"""
        return float(self.get_property(self.__section, 'SPECIFIC_INVESTMENT_COSTS_ENERGY_STORAGE'))

    @property
    def specific_investment_costs_power(self) -> float:
        """Returns specific_investment_costs for power electronics"""
        return float(self.get_property(self.__section, 'SPECIFIC_INVESTMENT_COSTS_POWER'))

    @property
    def operation_maintenance_costs_fix(self) -> float:
        """Returns fix operation_maintenance_costs"""
        return float(self.get_property(self.__section, 'OPERATION_MAINTENANCE_COSTS_FIX'))

    @property
    def balance_of_plant_costs(self) -> float:
        """Returns balance_of_plant_costs"""
        return float(self.get_property(self.__section, 'BALANCE_OF_PLANT_COSTS'))

    @property
    def use_system_costs(self) -> bool:
        """Returns choice of system or storage_/power_costs"""
        return self.get_property(self.__section, 'USE_SYSTEM_COSTS').lower() in TRUTH_CHECKER

    @property
    def use_user_defined_costs(self) -> bool:
        """Returns choice of user_defined_costs or cost fitting curve """
        return self.get_property(self.__section, 'USE_USER_DEFINED_COSTS').lower() in TRUTH_CHECKER

    @property
    def predefined_costs_scenario(self) -> [str]:
        """Returns the scenario mode"""
        return self.get_property(self.__section, 'PREDEFINED_COSTS_SCENARIO')

    @property
    def year_of_investment(self) -> float:
        """Returns the year_of_investment"""
        return float(self.get_property(self.__section, 'YEAR_OF_INVESTMENT'))

    @property
    def depreciation_period_storage(self) -> float:
        """Returns the depreciation period of the energy storage system"""
        return float(self.get_property(self.__section, 'DEPRECIATION_PERIOD_STORAGE'))

    @property
    def depreciation_period_grid(self) -> float:
        """Returns the depreciation period of the grid"""
        return float(self.get_property(self.__section, 'DEPRECIATION_PERIOD_GRID'))

    @property
    def discount_rate(self) -> float:
        """Returns the discount rate"""
        return float(self.get_property(self.__section, 'DISCOUNT_RATE'))